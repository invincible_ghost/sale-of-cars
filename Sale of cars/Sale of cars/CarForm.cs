﻿using Sale_of_cars.CarForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars
{
    public partial class CarForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        //Провірка загрузки форми
        public bool Loadedform = false;
        public CarForm()
        {
            InitializeComponent();
        }

        private void CarForm_Load(object sender, EventArgs e)
        {

            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            #region Заповнення Combobox
            //заполнение датасета
            string query1 = "Select * from [Cars Marks]";
            DataSet dataset1 = new DataSet();
            SqlDataAdapter adapter1 = new SqlDataAdapter(query1, db);
            adapter1.Fill(dataset1);
            //отображение в марка
            comboBox4.DataSource = dataset1.Tables[0];
            comboBox4.DisplayMember = "Name";
            comboBox4.ValueMember = "Id";

            //заполнение датасета
            string query2 = "Select distinct [Model].Id, [Model].[Name] from [Model] " +
                            "inner join [Cars Marks] on [Model].Id_mark = " + comboBox4.SelectedValue;
            DataSet dataset2 = new DataSet();
            SqlDataAdapter adapter2 = new SqlDataAdapter(query2, db);
            adapter2.Fill(dataset2);
            //отображение в модель
            comboBox5.DataSource = dataset2.Tables[0];
            comboBox5.DisplayMember = "Name";
            comboBox5.ValueMember = "Id";

            //заполнение датасета
            string query3 = "Select distinct [Body type].Id, [Body type].[Name] from [Body type]";
            DataSet dataset3 = new DataSet();
            SqlDataAdapter adapter3 = new SqlDataAdapter(query3, db);
            adapter3.Fill(dataset3);
            //отображение в кузов
            comboBox1.DataSource = dataset3.Tables[0];
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "Id";

            //заполнение датасета
            string query4 = "Select distinct [Discount].id, [Discount].[NameDiscount] from [Discount]";
            DataSet dataset4 = new DataSet();
            SqlDataAdapter adapter4 = new SqlDataAdapter(query4, db);
            adapter4.Fill(dataset4);
            //отображение в знижка
            comboBox6.DataSource = dataset4.Tables[0];
            comboBox6.DisplayMember = "NameDiscount";
            comboBox6.ValueMember = "id";

            //заполнение датасета
            string query5 = "Select distinct [Car color].id, [Car color].[Name] from [Car color]";
            DataSet dataset5 = new DataSet();
            SqlDataAdapter adapter5 = new SqlDataAdapter(query5, db);
            adapter5.Fill(dataset5);
            //отображение в колір
            comboBox2.DataSource = dataset5.Tables[0];
            comboBox2.DisplayMember = "Name";
            comboBox2.ValueMember = "id";

            //заполнение датасета
            string query6 = "Select distinct [Type fuel].id, [Type fuel].[Name] from [Type fuel]";
            DataSet dataset6 = new DataSet();
            SqlDataAdapter adapter6 = new SqlDataAdapter(query6, db);
            adapter6.Fill(dataset6);
            //отображение в тип палива
            comboBox3.DataSource = dataset6.Tables[0];
            comboBox3.DisplayMember = "Name";
            comboBox3.ValueMember = "id";
            #endregion

            #region Заповненн полів форми
            SqlDataAdapter da = new SqlDataAdapter("Select [Cars].[Mark], [Model].[Name], [Cars].Body_type_id, [Cars].Color_id, Year_of_issue, [Cars].Fuel_id, [Power], Price, [Costs_on_highway], [Costs_in_city], [Costs_in_mixed_cycle], [Num_avto], [Num_engine], [Num_chassis], [Num_body], [Series_of_tech_passport], [Tech_passport_of_date] From Cars"
            +" Inner join Model on Cars.Model = [Model].Id"
			+" Inner join [Car identification] on Cars.Identification_id = [Car identification].Id"
            +" where Cars.Id = " + DataFrame.CarID, db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Cars");

            comboBox4.SelectedValue = ds.Tables[0].Rows[0].ItemArray[0].ToString(); //Марка
            comboBox5.SelectedText = ds.Tables[0].Rows[0].ItemArray[1].ToString(); //Модель
            comboBox1.SelectedValue = ds.Tables[0].Rows[0].ItemArray[2].ToString(); //Кузов
            comboBox2.SelectedValue = ds.Tables[0].Rows[0].ItemArray[3].ToString(); //Колір
            textBox3.Text = ds.Tables[0].Rows[0].ItemArray[4].ToString(); //Рук випуску
            comboBox3.SelectedValue = ds.Tables[0].Rows[0].ItemArray[5].ToString(); //Тип палива
            textBox5.Text = ds.Tables[0].Rows[0].ItemArray[6].ToString(); //Потужність
            textBox4.Text = ds.Tables[0].Rows[0].ItemArray[7].ToString(); //Ціна
            textBox6.Text = ds.Tables[0].Rows[0].ItemArray[8].ToString(); //По шосе 
            textBox7.Text = ds.Tables[0].Rows[0].ItemArray[9].ToString(); //По місту
            textBox8.Text = ds.Tables[0].Rows[0].ItemArray[10].ToString(); //В змішаному
            textBox1.Text = ds.Tables[0].Rows[0].ItemArray[11].ToString(); //Номер авто
            textBox2.Text = ds.Tables[0].Rows[0].ItemArray[12].ToString(); //Номер двигуна
            textBox9.Text = ds.Tables[0].Rows[0].ItemArray[13].ToString(); //Номер шасі
            textBox10.Text = ds.Tables[0].Rows[0].ItemArray[14].ToString(); //Номер кузова
            textBox12.Text = ds.Tables[0].Rows[0].ItemArray[15].ToString(); //Серія 
            textBox11.Text = ds.Tables[0].Rows[0].ItemArray[16].ToString(); //Дата
            #endregion
            
            db.Close();

            Loadedform = true;
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Loadedform == true)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();

                //заполнение датасета
                string query3 = "Select distinct [Model].Id, [Model].[Name] from [Model] " +
                                "inner join [Cars Marks] on [Model].Id_mark = " + comboBox4.SelectedValue;

                DataSet dataset3 = new DataSet();
                SqlDataAdapter adapter3 = new SqlDataAdapter(query3, db);
                adapter3.Fill(dataset3);
                //отображение в comboBox1
                comboBox5.DataSource = dataset3.Tables[0];
                comboBox5.DisplayMember = "Name";
                comboBox5.ValueMember = "Id";

                db.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool CarChenged = false;
 
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd1 = new SqlCommand("Update Cars" +
            " Set [Cars].[Mark] = @Mark1 , [Cars].[Model] = @Model1, [Cars].Body_type_id = @Body_type_id1, [Cars].Color_id = @Color_id1, Year_of_issue = @Year_of_issue1"
            + " , [Cars].Fuel_id = @Fuel_id1, [Power] = @Power1, Price = @Price1, [Costs_on_highway] = @Costs_on_highway1, [Costs_in_city] = @Costs_in_city1, "
            + " [Costs_in_mixed_cycle] = @Costs_in_mixed_cycle1 where [Cars].Id = " + DataFrame.CarID, db);

            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "@Mark1";
            param1.Value = comboBox4.SelectedValue;
            param1.SqlDbType = SqlDbType.Int;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Model1";
            param1.Value = comboBox5.SelectedValue;
            param1.SqlDbType = SqlDbType.Int;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Body_type_id1";
            param1.Value = comboBox1.SelectedValue;
            param1.SqlDbType = SqlDbType.Int;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Color_id1";
            param1.Value = comboBox2.SelectedValue;
            param1.SqlDbType = SqlDbType.Int;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Year_of_issue1";
            param1.Value = textBox3.Text;
            param1.SqlDbType = SqlDbType.Int;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Price1";
            param1.Value = textBox4.Text;
            param1.SqlDbType = SqlDbType.Float;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Fuel_id1";
            param1.Value = comboBox3.SelectedValue;
            param1.SqlDbType = SqlDbType.Int;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Power1";
            param1.Value = textBox5.Text;
            param1.SqlDbType = SqlDbType.Float;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Costs_on_highway1";
            param1.Value = textBox6.Text;
            param1.SqlDbType = SqlDbType.Float;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Costs_in_city1";
            param1.Value = textBox7.Text;
            param1.SqlDbType = SqlDbType.Float;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Costs_in_mixed_cycle1";
            param1.Value = textBox8.Text;
            param1.SqlDbType = SqlDbType.Float;
            cmd1.Parameters.Add(param1);

            cmd1.ExecuteNonQuery();
            CarChenged = true;
            db.Close();

            if (CarChenged == true)
            {
                MessageBox.Show("Авто відредаговано", "Виконано", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
                string CurentCarInentification = "0";
                bool CarADD = false;
                //Добавлення ідентифікації авто
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();
                SqlCommand cmd = new SqlCommand("Insert into [Car identification] " +
                "(Num_avto , Num_engine, Num_chassis, Num_body, Series_of_tech_passport, Tech_passport_of_date) " +
                "Values (@Num_avto1 , @Num_engine1, @Num_chassis1, @Num_body1, @Series_of_tech_passport1, @Tech_passport_of_date1)", db);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@Num_avto1";
                param.Value = textBox1.Text;
                param.SqlDbType = SqlDbType.NVarChar;
                cmd.Parameters.Add(param);
                param = new SqlParameter();
                param.ParameterName = "@Num_engine1";
                param.Value = textBox2.Text;
                param.SqlDbType = SqlDbType.NVarChar;
                cmd.Parameters.Add(param);
                param = new SqlParameter();
                param.ParameterName = "@Num_chassis1";
                param.Value = textBox9.Text;
                param.SqlDbType = SqlDbType.NVarChar;
                cmd.Parameters.Add(param);
                param = new SqlParameter();
                param.ParameterName = "@Num_body1";
                param.Value = textBox10.Text;
                param.SqlDbType = SqlDbType.NVarChar;
                cmd.Parameters.Add(param);
                param = new SqlParameter();
                param.ParameterName = "@Series_of_tech_passport1";
                param.Value = textBox12.Text;
                param.SqlDbType = SqlDbType.NVarChar;
                cmd.Parameters.Add(param);
                param = new SqlParameter();
                param.ParameterName = "@Tech_passport_of_date1";
                param.Value = textBox11.Text;
                param.SqlDbType = SqlDbType.NVarChar;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
                db.Close();

                //Зчитування поточної ідентифікації авто
                SqlConnection db1 = new SqlConnection(ConnectionString);
                db1.Open();
                string query2 = "Select distinct [Car identification].Id from [Car identification] where [Car identification].Num_avto = " + textBox1.Text + " "
                + " and [Car identification].Num_body = " + textBox10.Text + " and [Car identification].Num_chassis = " + textBox9.Text + " and [Car identification].Num_engine = " + textBox2.Text + " and "
                + " [Car identification].Series_of_tech_passport = " + textBox12.Text + " ;"; //" and [Car identification].Tech_passport_of_date = " + textBox11.Text+" ;";
                DataSet dataset2 = new DataSet();
                SqlDataAdapter adapter2 = new SqlDataAdapter(query2, db1);
                adapter2.Fill(dataset2);
                //запис id ідентифікації
                CurentCarInentification = dataset2.Tables[0].Rows[0].ItemArray[0].ToString();
                db1.Close();

                //Добавлення авто
                SqlConnection db2 = new SqlConnection(ConnectionString);
                db2.Open();
                SqlCommand cmd1 = new SqlCommand("Insert into Cars" +
                "(Mark , Model , Identification_id , Body_type_id , Color_id , Year_of_issue, Price, Fuel_id, Power, Costs_on_highway, Costs_in_city, Costs_in_mixed_cycle, Discount_id)" +
                "Values (@Mark1 , @Model1, @Identification_id1, @Body_type_id1, @Color_id1, @Year_of_issue1,  @Price1, @Fuel_id1, @Power1, @Costs_on_highway1, @Costs_in_city1, @Costs_in_mixed_cycle1, @Discount_id1 )", db2);
                SqlParameter param1 = new SqlParameter();
                param1.ParameterName = "@Mark1";
                param1.Value = comboBox4.SelectedValue;
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Model1";
                param1.Value = comboBox5.SelectedValue;
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Identification_id1";
                param1.Value = Convert.ToInt32(CurentCarInentification);
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Body_type_id1";
                param1.Value = comboBox1.SelectedValue;
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Color_id1";
                param1.Value = comboBox2.SelectedValue;
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Year_of_issue1";
                param1.Value = textBox3.Text;
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Price1";
                param1.Value = textBox4.Text;
                param1.SqlDbType = SqlDbType.Float;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Fuel_id1";
                param1.Value = comboBox3.SelectedValue;
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Power1";
                param1.Value = textBox5.Text;
                param1.SqlDbType = SqlDbType.Float;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Costs_on_highway1";
                param1.Value = textBox6.Text;
                param1.SqlDbType = SqlDbType.Float;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Costs_in_city1";
                param1.Value = textBox7.Text;
                param1.SqlDbType = SqlDbType.Float;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Costs_in_mixed_cycle1";
                param1.Value = textBox8.Text;
                param1.SqlDbType = SqlDbType.Float;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Discount_id1";
                param1.Value = comboBox6.SelectedValue;
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                cmd1.ExecuteNonQuery();
                CarADD = true;
                db2.Close();

                if (CarADD == true)
	            {
		            MessageBox.Show("Нове авто додане в базу даних", "Виконано", MessageBoxButtons.OK, MessageBoxIcon.Information);
	            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox5_Click(object sender, EventArgs e)
        {
            if (Loadedform == true)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();

                //заполнение датасета
                string query2 = "Select distinct [Model].Id, [Model].[Name] from [Model] " +
                                "inner join [Cars Marks] on [Model].Id_mark = " + comboBox4.SelectedValue;
                DataSet dataset2 = new DataSet();
                SqlDataAdapter adapter2 = new SqlDataAdapter(query2, db);
                adapter2.Fill(dataset2);
                //отображение в модель
                comboBox5.DataSource = dataset2.Tables[0];
                comboBox5.DisplayMember = "Name";
                comboBox5.ValueMember = "Id";
                db.Close();
            }
        }

        private void додатиМодельToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CarModelForm NewForm = new CarModelForm();
            NewForm.ShowDialog();
        }

        private void додатиМаркуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CarMarkForm NewForm = new CarMarkForm();
            NewForm.ShowDialog();
        }

        private void кузоваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CarBodyForm NewForm = new CarBodyForm();
            NewForm.ShowDialog();
        }

        private void кольориToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CarColorForm NewForm = new CarColorForm();
            NewForm.ShowDialog();
        }

        private void типиПаливToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CarFuelForm NewForm = new CarFuelForm();
            NewForm.ShowDialog();
        }

        private void знижкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CarDiscountForm NewForm = new CarDiscountForm();
            NewForm.ShowDialog();
        }

      
    }
}
