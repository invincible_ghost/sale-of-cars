﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Sale_of_cars
{
    public partial class DocumentForm : Form
    {
        private string DocumentFileName = @"..\..\Document\Sales document.doc";
        private TXTextControl.StreamType DocumentStreamType = TXTextControl.StreamType.MSWord;
        Word._Application oWord = new Word.Application();
        public DocumentForm()
        {
            InitializeComponent();
        }



        //private _Document GetDoc(string path)
        //{
        //    _Document oDoc = oWord.Documents.Add(path);
        //    SetTemplate(oDoc);
        //    return oDoc;
        //}
        //// Замена закладки SECONDNAME на данные введенные в textBox
        //private void SetTemplate(Word._Document oDoc)
        //{
        //    oDoc.Bookmarks["Salon"].Range.Text = "qazxswedc";//textSecondName.Text;
        //    // если нужно заменять другие закладки, тогда копируем верхнюю строку изменяя на нужные параметры 

        //}


        private void DocumentForm_Load(object sender, EventArgs e)
        {

            //_Document oDoc = GetDoc(Environment.CurrentDirectory + @"..\..\Document\Sales document.doc");
            //oDoc.SaveAs(FileName: Environment.CurrentDirectory + @"..\..\Document\Sales document4.doc");
            //oDoc.Close();







            textControl1.Load(DocumentFileName, DocumentStreamType);
        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TXTextControl.SaveSettings SaveSettings = new TXTextControl.SaveSettings();

            if (DocumentFileName != "")
            {
                // save under same name and type
                textControl1.Save(@"..\..\Document\Sales document.doc", DocumentStreamType);
            }
            else
            {
                // save as..
                textControl1.Save(TXTextControl.StreamType.All, SaveSettings);
                DocumentFileName = SaveSettings.SavedFile;
                DocumentStreamType = SaveSettings.SavedStreamType;
                this.Text = DocumentFileName;
            }
        }
    }
}
