﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars.UserForms
{
    public partial class UserRolesForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        bool Loadedform = false;
        public UserRolesForm()
        {
            InitializeComponent();
        }

        private void UserRolesForm_Load(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            //заполнение датасета
            string query1 = "Select * from [Roles table]";
            DataSet dataset1 = new DataSet();
            SqlDataAdapter adapter1 = new SqlDataAdapter(query1, db);
            adapter1.Fill(dataset1);
            //отображение в марка
            comboBox1.DataSource = dataset1.Tables[0];
            comboBox1.DisplayMember = "Role_Name";
            comboBox1.ValueMember = "id";
            Loadedform = true;
            db.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd = new SqlCommand("Insert into [User Roles] " +
            "([User_id] , [Role_id]) " +
            "Values (@User_id1 , @Role_id1)", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@User_id1";
            param.Value = Convert.ToInt32(DataFrame.UserID);
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Role_id1";
            param.Value = Convert.ToInt32(comboBox1.SelectedValue.ToString());
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();
            db.Close();

            this.Close();
        }
    }
}
