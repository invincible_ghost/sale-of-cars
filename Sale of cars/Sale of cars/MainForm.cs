﻿using Sale_of_cars.CarForms;
using Sale_of_cars.Entities;
using Sale_of_cars.SaleForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars
{
    
    public partial class MainForm : Form
    {
        bool FormLoaded = false;

        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        
        public MainForm()
        {
            InitializeComponent();
        }

        public void DataGridLoadDataCars()
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("Select distinct Cars.Id, [Cars Marks].[Name], [Model].[Name], [Body type].[Name], [Car color].[Name], Year_of_issue, [Type fuel].[Name], [Power], Price, [Costs_on_highway], [Costs_in_city], [Costs_in_mixed_cycle], [Identification_id] From Cars"
            + " Inner join [Cars Marks] on Cars.[Mark] = [Cars Marks].Id"
            + " Inner join Model on Cars.Model = [Model].Id"
            + " Inner join [Body type] on Cars.Body_type_id = [Body type].Id"
            + " Inner join [Car color] on Cars.Color_id = [Car color].Id"
            + " Inner join [Type fuel] on Cars.Fuel_id = [Type fuel].Id", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Cars");
            CarsdataGrid.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            CarsdataGrid.Columns[0].Visible = false;
            CarsdataGrid.Columns[1].HeaderText = "Марка";
            CarsdataGrid.Columns[2].HeaderText = "Модель";
            CarsdataGrid.Columns[3].HeaderText = "Кузов";
            CarsdataGrid.Columns[4].HeaderText = "Колір";
            CarsdataGrid.Columns[5].HeaderText = "Рік випуску";
            CarsdataGrid.Columns[6].HeaderText = "Паливо";
            CarsdataGrid.Columns[7].HeaderText = "Потужність";
            CarsdataGrid.Columns[8].HeaderText = "Ціна";
            CarsdataGrid.Columns[9].HeaderText = "Росхід по шосе";
            CarsdataGrid.Columns[10].HeaderText = "Росхід по місту";
            CarsdataGrid.Columns[11].HeaderText = "Змішаний цикл";
            CarsdataGrid.Columns[12].Visible = false;
        }

        public void DataGridLoadDataSellingCars()
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("Select distinct Cars.Id, [Cars Marks].[Name], [Model].[Name], [Body type].[Name], [Car color].[Name], Year_of_issue, [Type fuel].[Name], Price From Cars"
            + " Inner join [Cars Marks] on Cars.[Mark] = [Cars Marks].Id"
            + " Inner join Model on Cars.Model = [Model].Id"
            + " Inner join [Body type] on Cars.Body_type_id = [Body type].Id"
            + " Inner join [Car color] on Cars.Color_id = [Car color].Id"
            + " Inner join [Type fuel] on Cars.Fuel_id = [Type fuel].Id", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Cars");
            SellingCarsdataGridView.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            SellingCarsdataGridView.Columns[0].Visible = false;
            SellingCarsdataGridView.Columns[1].HeaderText = "Марка";
            SellingCarsdataGridView.Columns[2].HeaderText = "Модель";
            SellingCarsdataGridView.Columns[3].HeaderText = "Кузов";
            SellingCarsdataGridView.Columns[4].HeaderText = "Колір";
            SellingCarsdataGridView.Columns[5].HeaderText = "Рік випуску";
            SellingCarsdataGridView.Columns[6].HeaderText = "Паливо";
            SellingCarsdataGridView.Columns[7].HeaderText = "Ціна";
        }

        public void DataGridLoadDataUsers()
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("select distinct [Users].id, [Users].[Name], [Users].Surname, [Users].Last_name, [Posts].[Name], "
            +"[Users].Passport, [Users].[Login], [Users].[Password], [Users].Email, [Users].Date_of_birth, [Users].Place_of_residence_id from [Users] "
            +"inner join [Posts] on [Users].Post_id = [Posts].id", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Users]");
            UsersGridView.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            UsersGridView.Columns[0].Visible = false;
            UsersGridView.Columns[1].HeaderText = "Імя";
            UsersGridView.Columns[2].HeaderText = "Прізвище";
            UsersGridView.Columns[3].HeaderText = "Побатькові";
            UsersGridView.Columns[4].HeaderText = "Посада";
            UsersGridView.Columns[5].HeaderText = "Паспортні дані";
            UsersGridView.Columns[6].HeaderText = "Логін";
            UsersGridView.Columns[7].HeaderText = "Пароль";
            UsersGridView.Columns[8].HeaderText = "Email";
            UsersGridView.Columns[9].HeaderText = "Дата народження";
            UsersGridView.Columns[10].Visible = false;
        }

        public void DataGridLoadDataCustomers()
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("select distinct [Customers].[Id], [Customers].[Name], [Customers].Surname, [Customers].Last_name,"
            +" [Customers].Home_num, [Customers].Phone_num, [Customers].Date_of_birth, [Place of residence].country, [Place of residence].region,"
            +" [Place of residence].district, [Place of residence].village, [Place of residence].city, [Place of residence].street, "
            + " [Place of residence].apartment_house, [Customers].[Place_of_residence_id] from [Customers]"
            +" inner join [Place of residence] on [Customers].Place_of_residence_id = [Place of residence].id", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Customers]");
            CustomersGridView.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            CustomersGridView.Columns[0].Visible = false;
            CustomersGridView.Columns[1].HeaderText = "Імя";
            CustomersGridView.Columns[2].HeaderText = "Прізвище";
            CustomersGridView.Columns[3].HeaderText = "Побатькові";
            CustomersGridView.Columns[4].HeaderText = "Домашній тл.";
            CustomersGridView.Columns[5].HeaderText = "Мобільний тл.";
            CustomersGridView.Columns[6].HeaderText = "Дата народження";
            CustomersGridView.Columns[7].HeaderText = "Країна";
            CustomersGridView.Columns[8].HeaderText = "Область";
            CustomersGridView.Columns[9].HeaderText = "Район";
            CustomersGridView.Columns[10].HeaderText = "Село";
            CustomersGridView.Columns[11].HeaderText = "Місто";
            CustomersGridView.Columns[12].HeaderText = "Вулиця";
            CustomersGridView.Columns[13].HeaderText = "Будинок";
            CustomersGridView.Columns[14].Visible = false;
        }

        public void DataGridLoadDataSellingCustomers()
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("select distinct [Customers].[Id], [Customers].[Name], [Customers].Surname, [Customers].Last_name,"
            + " [Customers].Date_of_birth from [Customers]", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Customers]");
            SellingCustomersdataGridView.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            SellingCustomersdataGridView.Columns[0].Visible = false;
            SellingCustomersdataGridView.Columns[1].HeaderText = "Імя";
            SellingCustomersdataGridView.Columns[2].HeaderText = "Прізвище";
            SellingCustomersdataGridView.Columns[3].HeaderText = "Побатькові";
            SellingCustomersdataGridView.Columns[4].HeaderText = "Дата народження";
        }

        public void DataGridLoadDataCarsToEquipment()
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("Select distinct Cars.Id, [Cars Marks].[Name], [Model].[Name], [Body type].[Name], [Car color].[Name], Year_of_issue, [Type fuel].[Name], [Power], Price From Cars"
            + " Inner join [Cars Marks] on Cars.[Mark] = [Cars Marks].Id"
            + " Inner join Model on Cars.Model = [Model].Id"
            + " Inner join [Body type] on Cars.Body_type_id = [Body type].Id"
            + " Inner join [Car color] on Cars.Color_id = [Car color].Id"
            + " Inner join [Type fuel] on Cars.Fuel_id = [Type fuel].Id", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Cars");
            CarEquipmentGridView.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            CarEquipmentGridView.Columns[0].Visible = false;
            CarEquipmentGridView.Columns[1].HeaderText = "Марка";
            CarEquipmentGridView.Columns[2].HeaderText = "Модель";
            CarEquipmentGridView.Columns[3].HeaderText = "Кузов";
            CarEquipmentGridView.Columns[4].HeaderText = "Колір";
            CarEquipmentGridView.Columns[5].HeaderText = "Рік випуску";
            CarEquipmentGridView.Columns[6].HeaderText = "Паливо";
            CarEquipmentGridView.Columns[7].HeaderText = "Потужність";
            CarEquipmentGridView.Columns[8].HeaderText = "Ціна";

        }

        public void DataGridLoadDataEquipment(string IDCar)
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("Select [Additional equipment].id, [Additional equipment].[Name], [Additional equipment].Price from [Cars]"
            + " inner join [Car equipment] on [Cars].Id = [Car equipment].Car_id"
            + " inner join [Additional equipment] on [Car equipment].Equipment_id = [Additional equipment].id "
            + " where Cars.Id = " + IDCar, db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Cars");
            EquipmentdataGridView.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            EquipmentdataGridView.Columns[0].Visible = false;
            EquipmentdataGridView.Columns[1].HeaderText = "Назва";
            EquipmentdataGridView.Columns[2].HeaderText = "Ціна";

        }

        public void DataGridLoadDataSelectedSellingEquipment()
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("Select * from [Equipments Frame]", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Equipments Frame]");
            SelectedSellingEquipdataGridView.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            SelectedSellingEquipdataGridView.Columns[0].Visible = false;
            SelectedSellingEquipdataGridView.Columns[1].Visible = false;
            SelectedSellingEquipdataGridView.Columns[2].HeaderText = "Назва";
            SelectedSellingEquipdataGridView.Columns[3].HeaderText = "Ціна";

        }

        public void DataGridLoadDataForSellingEquipment(string IDCar)
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("Select [Additional equipment].id, [Additional equipment].[Name], [Additional equipment].Price from [Cars]"
            + " inner join [Car equipment] on [Cars].Id = [Car equipment].Car_id"
            + " inner join [Additional equipment] on [Car equipment].Equipment_id = [Additional equipment].id "
            + " where Cars.Id = " + IDCar, db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Cars");
            SellingEquipdataGridView.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            SellingEquipdataGridView.Columns[0].Visible = false;
            SellingEquipdataGridView.Columns[1].HeaderText = "Назва";
            SellingEquipdataGridView.Columns[2].HeaderText = "Ціна";

        }

        public void DataGridLoadDataSellingConditions()
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("Select * from [Conditions Frame] ", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Conditions Frame]");
            dataGridView1.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].Visible = false;
            dataGridView1.Columns[2].HeaderText = "Назва";
            dataGridView1.Columns[3].HeaderText = "Ціна";

        }

        public void FunctionOfCarPriceWithEquipment()
        { 
            float sumpricequip = 0.0f;

            if (SelectedSellingEquipdataGridView.RowCount != 0)
            {
                for (int i = 0; i < SelectedSellingEquipdataGridView.RowCount; i++)
                {
                    sumpricequip += Convert.ToSingle(SelectedSellingEquipdataGridView.Rows[i].Cells[3].Value.ToString());
                }
            }
            textBox17.Text = Convert.ToString(Convert.ToSingle(SellingCarsdataGridView.CurrentRow.Cells[7].Value.ToString().Trim()) + sumpricequip);
        }

        public void FunctionOfDocPrice()
        {
            float sumpricequip = 0.0f;

            if (SelectedSellingEquipdataGridView.RowCount != 0)
            {
                for (int i = 0; i < SelectedSellingEquipdataGridView.RowCount; i++)
                {
                    sumpricequip += Convert.ToSingle(SelectedSellingEquipdataGridView.Rows[i].Cells[3].Value.ToString());
                }
            }

            if (dataGridView1.RowCount != 0)
            {
                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    sumpricequip += Convert.ToSingle(dataGridView1.Rows[i].Cells[3].Value.ToString());
                }
            }
           textBox18.Text = Convert.ToString(Convert.ToSingle(SellingCarsdataGridView.CurrentRow.Cells[7].Value.ToString().Trim()) + sumpricequip);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //#region Заглушка під логінацію
            //CurentUser.ID = "1";
            //CurentUser.Name = "1";
            //#endregion

            NameStatusLabel.Text = "Користувач системи " + CurentUser.Name.ToString();


            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            SqlDataAdapter da = new SqlDataAdapter("select distinct [Users].id, [Users].[Name], [Users].Surname, [Users].Last_name, [Users].Date_of_birth from [Users]"
            + " where [Users].id = " + CurentUser.ID, db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Users");

            textBox5.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString(); //Імя
            textBox6.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString(); //Прізвище
            textBox7.Text = ds.Tables[0].Rows[0].ItemArray[3].ToString(); //Побатькові
            textBox8.Text = ds.Tables[0].Rows[0].ItemArray[4].ToString(); //Дата народження

            db.Close();

            //Вивід авто
            DataGridLoadDataCars();

            //Вивід авто комплектації
            DataGridLoadDataCarsToEquipment();

            //Вивід користувачів
            DataGridLoadDataUsers();

            //Вивід клієнтів
            DataGridLoadDataCustomers();

            //Вивід клієнтів для продажу авто
            DataGridLoadDataSellingCustomers();
            
            //Вивід авто для продажу авто
            DataGridLoadDataSellingCars();

            //Виділення першої строчки першої колонки гріда при загрузці сторінки
            CarsdataGrid.CurrentCell = CarsdataGrid.Rows[0].Cells[1];
            CarEquipmentGridView.CurrentCell = CarEquipmentGridView.Rows[0].Cells[1];

            FormLoaded = true;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void ChangeCar_Click(object sender, EventArgs e)
        {
            DataFrame.CarID = CarsdataGrid.CurrentRow.Cells[0].Value.ToString();
            DataFrame.Car_Identification_id = CarsdataGrid.CurrentRow.Cells[12].Value.ToString();

            CarForm F1 = new CarForm();
            F1.ShowDialog();
            
        }

        private void DeleteCar_Click(object sender, EventArgs e)
        {
            string CarDel = CarsdataGrid.CurrentRow.Cells[0].Value.ToString();
            string CarIdentitiDel = CarsdataGrid.CurrentRow.Cells[12].Value.ToString();
            DialogResult dialogResult = MessageBox.Show("Ви впевнен що хочете видалити авто " + CarsdataGrid.CurrentRow.Cells[1].Value.ToString().Trim() + " " + CarsdataGrid.CurrentRow.Cells[2].Value.ToString().Trim() + " з бази даних?", "Видалення авто", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();

                SqlCommand cmd1 = new SqlCommand("Delete From Cars" +
                " where Cars.Id = @IDАвто", db);
                SqlParameter param1 = new SqlParameter();
                param1.ParameterName = "@IDАвто";
                param1.Value = Convert.ToInt32(CarDel);
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd = new SqlCommand("Delete From [Car identification]" +
                " where [Car identification].Id = @IDАвтоІдентифікація", db);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@IDАвтоІдентифікація";
                param.Value = Convert.ToInt32(CarIdentitiDel);
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();

                db.Close();

                DataGridLoadDataCars();
            }
            else if (dialogResult == DialogResult.No)
            {
               
            }
        }

        private void MainForm_Activated(object sender, EventArgs e)
        {
            //Вивід(рефреш) авто
            DataGridLoadDataCars();

            //Вивід(рефреш) авто комплектації
            DataGridLoadDataCarsToEquipment();

            //Вивід(рефреш) користувачів
            DataGridLoadDataUsers();

            //Вивід(рефреш) клієнтів
            DataGridLoadDataCustomers();

            //Вивід(рефреш) послуг
            DataGridLoadDataSellingConditions();

            //Вивід(рефреш) комплектації
            //DataGridLoadDataEquipment(CurentCar.ID);
        }

        private void CarControlAll_Click(object sender, EventArgs e)
        {

        }

        private void CarComplectControl_Click(object sender, EventArgs e)
        {

        }

        private void CarEquipmentGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string CarCheched = CarEquipmentGridView.CurrentRow.Cells[0].Value.ToString();

            DataGridLoadDataEquipment(CarCheched);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string CarChecked = CarEquipmentGridView.CurrentRow.Cells[0].Value.ToString();
            string EquipmentChecked = EquipmentdataGridView.CurrentRow.Cells[0].Value.ToString();
            DialogResult dialogResult = MessageBox.Show("Ви впевнен що хочете видалити комплектацію '" + EquipmentdataGridView.CurrentRow.Cells[1].Value.ToString().Trim() + "' для автомобіля " + CarEquipmentGridView.CurrentRow.Cells[1].Value.ToString().Trim() + " " + CarEquipmentGridView.CurrentRow.Cells[2].Value.ToString().Trim() + " з бази даних?", "Видалення комплектації авто", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();
                SqlCommand cmd = new SqlCommand("Delete CE From [Car equipment] CE"
                    +" INNER JOIN [Additional equipment] AE"
                    +" on CE.Equipment_id = AE.id"
                    +" INNER JOIN Cars Car"
                    +" on CE.Car_id = Car.Id"
                    + " where CE.Equipment_id = @IDКомплектації AND CE.Car_id = @IDАвто", db);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@IDКомплектації";
                param.Value = Convert.ToInt32(EquipmentChecked);
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);
                param = new SqlParameter();
                param.ParameterName = "@IDАвто";
                param.Value = Convert.ToInt32(CarChecked);
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();

                db.Close();

                DataGridLoadDataEquipment(CarChecked);
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataFrame.CarID = CarEquipmentGridView.CurrentRow.Cells[0].Value.ToString();
            CarEquipmentForm NewForm = new CarEquipmentForm();
            NewForm.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            EquipmentForm NewForm = new EquipmentForm();
            NewForm.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DataFrame.UserID = UsersGridView.CurrentRow.Cells[0].Value.ToString();
            DataFrame.UserResidenceID = UsersGridView.CurrentRow.Cells[10].Value.ToString();
            UserForm NewForm = new UserForm();
            NewForm.ShowDialog();
        }

        private void проПрограмуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutProgram NewForm = new AboutProgram();
            NewForm.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string UserDel = UsersGridView.CurrentRow.Cells[0].Value.ToString();
            string UserResidenceDel = UsersGridView.CurrentRow.Cells[10].Value.ToString();
            DialogResult dialogResult = MessageBox.Show("Ви впевнен що хочете видалити користувача " + UsersGridView.CurrentRow.Cells[1].Value.ToString().Trim() + " " + UsersGridView.CurrentRow.Cells[2].Value.ToString().Trim() + " " + UsersGridView.CurrentRow.Cells[3].Value.ToString().Trim() + " з бази даних?", "Видалення користувача", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();

                SqlCommand cmd1 = new SqlCommand("Delete From [Users]" +
                " where [Users].id = @IDКористувача", db);
                SqlParameter param1 = new SqlParameter();
                param1.ParameterName = "@IDКористувача";
                param1.Value = Convert.ToInt32(UserDel);
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd = new SqlCommand("Delete From [Place of residence]" +
                " where [Place of residence].id = @IDПроживанняКористувача", db);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@IDПроживанняКористувача";
                param.Value = Convert.ToInt32(UserResidenceDel);
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();

                db.Close();

                DataGridLoadDataUsers();
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DataFrame.CustomerID = CustomersGridView.CurrentRow.Cells[0].Value.ToString();
            DataFrame.CustomerResidenceID = CustomersGridView.CurrentRow.Cells[14].Value.ToString();
            CustomerForm NewForm = new CustomerForm();
            NewForm.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string CustomerDel = CustomersGridView.CurrentRow.Cells[0].Value.ToString();
            string CustomerResidenceDel = CustomersGridView.CurrentRow.Cells[14].Value.ToString();
            DialogResult dialogResult = MessageBox.Show("Ви впевнен що хочете видалити клієнта " + CustomersGridView.CurrentRow.Cells[1].Value.ToString().Trim() + " " + CustomersGridView.CurrentRow.Cells[2].Value.ToString().Trim() + " " + CustomersGridView.CurrentRow.Cells[3].Value.ToString().Trim() + " з бази даних?", "Видалення клієнта", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();

                SqlCommand cmd1 = new SqlCommand("Delete From [Customers]" +
                " where [Customers].Id = @IDКлієнта", db);
                SqlParameter param1 = new SqlParameter();
                param1.ParameterName = "@IDКлієнта";
                param1.Value = Convert.ToInt32(CustomerDel);
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd = new SqlCommand("Delete From [Place of residence]" +
                " where [Place of residence].id = @IDПроживанняКлієнта", db);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@IDПроживанняКлієнта";
                param.Value = Convert.ToInt32(CustomerResidenceDel);
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();

                db.Close();

                DataGridLoadDataCustomers();
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DocumentForm NewForm = new DocumentForm();
            NewForm.ShowDialog();
        }

        private void SellingCarsdataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string CarCheched = SellingCarsdataGridView.CurrentRow.Cells[0].Value.ToString();

            DataGridLoadDataForSellingEquipment(CarCheched);

            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            SqlDataAdapter da = new SqlDataAdapter("Select distinct Cars.Id, [Cars Marks].[Name], [Model].[Name], [Body type].[Name], [Car color].[Name], Year_of_issue, [Type fuel].[Name], [Power], Price From Cars"
            + " Inner join [Cars Marks] on Cars.[Mark] = [Cars Marks].Id"
            + " Inner join Model on Cars.Model = [Model].Id"
            + " Inner join [Body type] on Cars.Body_type_id = [Body type].Id"
            + " Inner join [Car color] on Cars.Color_id = [Car color].Id"
            + " Inner join [Type fuel] on Cars.Fuel_id = [Type fuel].Id"
            + " where Cars.Id = " + SellingCarsdataGridView.CurrentRow.Cells[0].Value.ToString(), db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Cars");

            textBox9.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString(); //Марка
            textBox10.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString(); //Модель
            textBox14.Text = ds.Tables[0].Rows[0].ItemArray[3].ToString(); //Кузов
            textBox13.Text = ds.Tables[0].Rows[0].ItemArray[4].ToString(); //Колір
            textBox11.Text = ds.Tables[0].Rows[0].ItemArray[5].ToString(); //Рук випуску
            textBox12.Text = ds.Tables[0].Rows[0].ItemArray[6].ToString(); //Тип палива
            textBox15.Text = ds.Tables[0].Rows[0].ItemArray[7].ToString(); //Потужність
            textBox16.Text = ds.Tables[0].Rows[0].ItemArray[8].ToString(); //Ціна

            //Очистака гріда комплектацій
                SqlCommand cmd = new SqlCommand("Delete From [Equipments Frame]", db);
                cmd.ExecuteNonQuery();

            db.Close();

            DataGridLoadDataSelectedSellingEquipment();

            //Очистака полів прорахування цін
            textBox17.Text = "";
            textBox18.Text = "";

            FunctionOfDocPrice();
        }

        private void SellingCustomersdataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("select distinct [Customers].[Id], [Customers].[Name], [Customers].Surname, [Customers].Last_name,"
            + " [Customers].Date_of_birth from [Customers] where [Customers].[Id] = "+SellingCustomersdataGridView.CurrentRow.Cells[0].Value.ToString(), db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Customers]");

            textBox1.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString(); //Імя
            textBox2.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString(); //Прізвище
            textBox3.Text = ds.Tables[0].Rows[0].ItemArray[3].ToString(); //Побатькові
            textBox4.Text = ds.Tables[0].Rows[0].ItemArray[4].ToString(); //Дата народження

            db.Close();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            //Добавлення
            if (SellingEquipdataGridView.CurrentCell != null)
            {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd = new SqlCommand("Insert into [Equipments Frame] " +
            "([Equip_id], [Equip_Name], [Equip_Price]) " +
            "Values (@Equip_id1, @Equip_Name1, @Equip_Price1)", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@Equip_id1";
            param.Value = Convert.ToInt32(SellingEquipdataGridView.CurrentRow.Cells[0].Value.ToString());
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Equip_Name1";
            param.Value = SellingEquipdataGridView.CurrentRow.Cells[1].Value.ToString();
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Equip_Price1";
            param.Value = Convert.ToSingle(SellingEquipdataGridView.CurrentRow.Cells[2].Value.ToString());
            param.SqlDbType = SqlDbType.Float;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();
            db.Close();

            DataGridLoadDataSelectedSellingEquipment();

            FunctionOfCarPriceWithEquipment();
            FunctionOfDocPrice();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            string EquipDel = SelectedSellingEquipdataGridView.CurrentRow.Cells[0].Value.ToString();
            DialogResult dialogResult = MessageBox.Show("Ви впевнен що хочете видалити комплектацію '" + SelectedSellingEquipdataGridView.CurrentRow.Cells[2].Value.ToString().Trim() + "'?", "Видалення комплектації", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();

                SqlCommand cmd = new SqlCommand("Delete From [Equipments Frame]" +
                " where [Equipments Frame].id = @IDКомплектації", db);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@IDКомплектації";
                param.Value = Convert.ToInt32(EquipDel);
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();

                db.Close();

                DataGridLoadDataSelectedSellingEquipment();
            }
            else if (dialogResult == DialogResult.No)
            {

            }
            FunctionOfCarPriceWithEquipment();
            FunctionOfDocPrice();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            SaleConditionsForm NewForm = new SaleConditionsForm();
            NewForm.ShowDialog();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            string CondDel = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            DialogResult dialogResult = MessageBox.Show("Ви впевнен що хочете видалити послугу '" + dataGridView1.CurrentRow.Cells[2].Value.ToString().Trim()+ "' з бази даних?", "Видалення послуги", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();

                SqlCommand cmd = new SqlCommand("Delete From [Conditions Frame]" +
                " where [Conditions Frame].id = @IDПослуги", db);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@IDПослуги";
                param.Value = Convert.ToInt32(CondDel);
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();

                db.Close();

                DataGridLoadDataSellingConditions();
            }
            else if (dialogResult == DialogResult.No)
            {

            }
            FunctionOfDocPrice();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            SaleAllDocForm NewForm = new SaleAllDocForm();
            NewForm.ShowDialog();
        }

        //Формування договору про продаж
        private void button13_Click(object sender, EventArgs e)
        {
           
            DateTime datetime = DateTime.Now;
            DateTime SaveDate = datetime;
            string CurentDoc = "";
            //Добавлення договору
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd = new SqlCommand("Insert into [Sales documents]" +
            "([User_id],[Car_id],[Customer_id],[Data_of_signing],[Signing_price]) " +
            "Values (@User_id1, @Car_id1, @Customer_id1, @Data_of_signing1, @Signing_price1)", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@User_id1";
            param.Value = Convert.ToInt32(CurentUser.ID);
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Car_id1";
            param.Value = Convert.ToInt32(SellingCarsdataGridView.CurrentRow.Cells[0].Value.ToString());
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Customer_id1";
            param.Value = Convert.ToInt32(SellingCustomersdataGridView.CurrentRow.Cells[0].Value.ToString());
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Data_of_signing1";
            param.Value = SaveDate;
            param.SqlDbType = SqlDbType.Date;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Signing_price1";
            param.Value = Convert.ToSingle(textBox18.Text);
            param.SqlDbType = SqlDbType.Float;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();
            db.Close();

            //Зчитування id поточного договору
            SqlConnection db1 = new SqlConnection(ConnectionString);
            db1.Open();
            string query2 = "Select distinct [Sales documents].id from [Sales documents] where [Sales documents].[User_id] = " + CurentUser.ID
            + " and [Sales documents].[Car_id] = " + SellingCarsdataGridView.CurrentRow.Cells[0].Value.ToString()
            + " and  [Sales documents].[Customer_id] = " + SellingCustomersdataGridView.CurrentRow.Cells[0].Value.ToString();// + " and [Sales documents].[Data_of_signing] = " + SaveDate + " ";
            DataSet dataset2 = new DataSet();
            SqlDataAdapter adapter2 = new SqlDataAdapter(query2, db1);
            adapter2.Fill(dataset2);
            //запис id ідентифікації
            CurentDoc = dataset2.Tables[0].Rows[0].ItemArray[0].ToString();
            db1.Close();


            if (dataGridView1.RowCount != 0)
            {
                for (int i = 0; i < dataGridView1.RowCount; i++)
                {

                    SqlConnection db2 = new SqlConnection(ConnectionString);
                    db2.Open();
                    SqlCommand cmd2 = new SqlCommand("Insert into [Sale conditions]" +
                    "([Document_id],[Condition_id]) " +
                    "Values (@Document_id1, @Condition_id1)", db2);
                    SqlParameter param2 = new SqlParameter();
                    param2.ParameterName = "@Document_id1";
                    param2.Value = Convert.ToInt32(CurentDoc);
                    param2.SqlDbType = SqlDbType.Int;
                    cmd2.Parameters.Add(param2);
                    param2 = new SqlParameter();
                    param2.ParameterName = "@Condition_id1";
                    param2.Value = Convert.ToInt32(dataGridView1.Rows[i].Cells[1].Value.ToString());
                    param2.SqlDbType = SqlDbType.Int;
                    cmd2.Parameters.Add(param2);
                    cmd2.ExecuteNonQuery();
                    db2.Close();
                }
            }

            if (SelectedSellingEquipdataGridView.RowCount != 0)
            {
                for (int i = 0; i < SelectedSellingEquipdataGridView.RowCount; i++)
                {

                    SqlConnection db3 = new SqlConnection(ConnectionString);
                    db3.Open();
                    SqlCommand cmd3 = new SqlCommand("Insert into [Sale Car Equipment]" +
                    "([Sales_document_id],[Additional_equipment_id]) " +
                    "Values (@Sales_document_id1, @Additional_equipment_id1)", db3);
                    SqlParameter param3 = new SqlParameter();
                    param3.ParameterName = "@Sales_document_id1";
                    param3.Value = Convert.ToInt32(CurentDoc);
                    param3.SqlDbType = SqlDbType.Int;
                    cmd3.Parameters.Add(param3);
                    param3 = new SqlParameter();
                    param3.ParameterName = "@Additional_equipment_id1";
                    param3.Value = Convert.ToInt32(SelectedSellingEquipdataGridView.Rows[i].Cells[1].Value.ToString());
                    param3.SqlDbType = SqlDbType.Int;
                    cmd3.Parameters.Add(param3);
                    cmd3.ExecuteNonQuery();
                    db3.Close();
                }
            }

            DialogResult dialogResult = MessageBox.Show("Ви оформили новий договір. Вітаю!", "Оформмлення договору", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginForm NewForm = new LoginForm();
            NewForm.ShowDialog(); 
        }
    }
}
