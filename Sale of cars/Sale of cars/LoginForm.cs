﻿using Sale_of_cars.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars
{
    
    public partial class LoginForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        public LoginForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox1.Text != "" || textBox2.Text != "")
            {
                SqlConnection bd = new SqlConnection(ConnectionString);

                bd.Open();
                //превірка на існування логіна в базі даних
                int Check = 0;
                SqlCommand cmd = new SqlCommand("Select * From Users", bd);
                using (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    for (int i = 0; i < dr.FieldCount; i++)
                        while (dr.Read())
                        {
                            if (textBox1.Text == dr.GetValue(6).ToString().Trim() && textBox2.Text == dr.GetValue(7).ToString().Trim())
                            {
                                Check = 1;

                                CurentUser.ID = dr.GetValue(0).ToString().Trim();
                                CurentUser.Name = dr.GetValue(3).ToString().Trim() + " "+dr.GetValue(1).ToString().Trim();
                                CurentUser.Pass = textBox2.Text;

                                this.Hide();
                                MainForm F1 = new MainForm();
                                F1.ShowDialog();
                            }
                        }
                }
                bd.Close();

                if (Check == 0)
                {
                    MessageBox.Show(
                               "Неправильні данні!",
                               "Помилка");
                }
            }
            else
            {
                MessageBox.Show(
                "Введіть данні!",
                "Помилка");
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
                textBox2.PasswordChar = '*';
            else
                textBox2.PasswordChar = '\0';
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {

        }
    }
}
