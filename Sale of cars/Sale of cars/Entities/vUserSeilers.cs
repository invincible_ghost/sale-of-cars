//------------------------------------------------------------------------------
// <auto-generated>
//    Этот код был создан из шаблона.
//
//    Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//    Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sale_of_cars.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class vUserSeilers
    {
        public int id { get; set; }
        public int Car_id { get; set; }
        public int Customer_id { get; set; }
        public System.DateTime Data_of_signing { get; set; }
        public double Signing_price { get; set; }
    }
}
