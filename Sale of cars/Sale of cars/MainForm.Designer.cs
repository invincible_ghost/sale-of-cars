﻿namespace Sale_of_cars
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.вихідToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.проПрограмуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ControlPanel = new System.Windows.Forms.TabControl();
            this.CarControl = new System.Windows.Forms.TabPage();
            this.CarsControlPanel = new System.Windows.Forms.TabControl();
            this.CarControlAll = new System.Windows.Forms.TabPage();
            this.ChangeCar = new System.Windows.Forms.Button();
            this.DeleteCar = new System.Windows.Forms.Button();
            this.CarsdataGrid = new System.Windows.Forms.DataGridView();
            this.CarComplectControl = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.EquipmentdataGridView = new System.Windows.Forms.DataGridView();
            this.CarEquipmentGridView = new System.Windows.Forms.DataGridView();
            this.UsersControl = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.UsersGridView = new System.Windows.Forms.DataGridView();
            this.CustomersControl = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.CustomersGridView = new System.Windows.Forms.DataGridView();
            this.SaleControl = new System.Windows.Forms.TabPage();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.SelectedSellingEquipdataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.SellingEquipdataGridView = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SellingCarsdataGridView = new System.Windows.Forms.DataGridView();
            this.SellingCustomersdataGridView = new System.Windows.Forms.DataGridView();
            this.button8 = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.NameStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.databaseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            this.CarControl.SuspendLayout();
            this.CarsControlPanel.SuspendLayout();
            this.CarControlAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CarsdataGrid)).BeginInit();
            this.CarComplectControl.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentdataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarEquipmentGridView)).BeginInit();
            this.UsersControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UsersGridView)).BeginInit();
            this.CustomersControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustomersGridView)).BeginInit();
            this.SaleControl.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedSellingEquipdataGridView)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SellingEquipdataGridView)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SellingCarsdataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellingCustomersdataGridView)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.databaseBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.вихідToolStripMenuItem,
            this.проПрограмуToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1060, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // вихідToolStripMenuItem
            // 
            this.вихідToolStripMenuItem.Name = "вихідToolStripMenuItem";
            this.вихідToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.вихідToolStripMenuItem.Text = "Вихід";
            this.вихідToolStripMenuItem.Click += new System.EventHandler(this.вихідToolStripMenuItem_Click);
            // 
            // проПрограмуToolStripMenuItem
            // 
            this.проПрограмуToolStripMenuItem.Name = "проПрограмуToolStripMenuItem";
            this.проПрограмуToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.проПрограмуToolStripMenuItem.Text = "Про програму";
            this.проПрограмуToolStripMenuItem.Click += new System.EventHandler(this.проПрограмуToolStripMenuItem_Click);
            // 
            // ControlPanel
            // 
            this.ControlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ControlPanel.Controls.Add(this.CarControl);
            this.ControlPanel.Controls.Add(this.UsersControl);
            this.ControlPanel.Controls.Add(this.CustomersControl);
            this.ControlPanel.Controls.Add(this.SaleControl);
            this.ControlPanel.Location = new System.Drawing.Point(0, 27);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.SelectedIndex = 0;
            this.ControlPanel.Size = new System.Drawing.Size(1060, 513);
            this.ControlPanel.TabIndex = 1;
            // 
            // CarControl
            // 
            this.CarControl.Controls.Add(this.CarsControlPanel);
            this.CarControl.Location = new System.Drawing.Point(4, 22);
            this.CarControl.Name = "CarControl";
            this.CarControl.Padding = new System.Windows.Forms.Padding(3);
            this.CarControl.Size = new System.Drawing.Size(1052, 487);
            this.CarControl.TabIndex = 0;
            this.CarControl.Text = "Авто";
            this.CarControl.UseVisualStyleBackColor = true;
            // 
            // CarsControlPanel
            // 
            this.CarsControlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CarsControlPanel.Controls.Add(this.CarControlAll);
            this.CarsControlPanel.Controls.Add(this.CarComplectControl);
            this.CarsControlPanel.Location = new System.Drawing.Point(0, 0);
            this.CarsControlPanel.Name = "CarsControlPanel";
            this.CarsControlPanel.SelectedIndex = 0;
            this.CarsControlPanel.Size = new System.Drawing.Size(1056, 488);
            this.CarsControlPanel.TabIndex = 0;
            // 
            // CarControlAll
            // 
            this.CarControlAll.Controls.Add(this.ChangeCar);
            this.CarControlAll.Controls.Add(this.DeleteCar);
            this.CarControlAll.Controls.Add(this.CarsdataGrid);
            this.CarControlAll.Location = new System.Drawing.Point(4, 22);
            this.CarControlAll.Name = "CarControlAll";
            this.CarControlAll.Padding = new System.Windows.Forms.Padding(3);
            this.CarControlAll.Size = new System.Drawing.Size(1048, 462);
            this.CarControlAll.TabIndex = 0;
            this.CarControlAll.Text = "Авто";
            this.CarControlAll.UseVisualStyleBackColor = true;
            this.CarControlAll.Click += new System.EventHandler(this.CarControlAll_Click);
            // 
            // ChangeCar
            // 
            this.ChangeCar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ChangeCar.Location = new System.Drawing.Point(839, 433);
            this.ChangeCar.Name = "ChangeCar";
            this.ChangeCar.Size = new System.Drawing.Size(123, 23);
            this.ChangeCar.TabIndex = 3;
            this.ChangeCar.Text = "Редагувати/Додати";
            this.ChangeCar.UseVisualStyleBackColor = true;
            this.ChangeCar.Click += new System.EventHandler(this.ChangeCar_Click);
            // 
            // DeleteCar
            // 
            this.DeleteCar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteCar.Location = new System.Drawing.Point(968, 433);
            this.DeleteCar.Name = "DeleteCar";
            this.DeleteCar.Size = new System.Drawing.Size(75, 23);
            this.DeleteCar.TabIndex = 2;
            this.DeleteCar.Text = "Видалити";
            this.DeleteCar.UseVisualStyleBackColor = true;
            this.DeleteCar.Click += new System.EventHandler(this.DeleteCar_Click);
            // 
            // CarsdataGrid
            // 
            this.CarsdataGrid.AllowUserToAddRows = false;
            this.CarsdataGrid.AllowUserToDeleteRows = false;
            this.CarsdataGrid.AllowUserToResizeColumns = false;
            this.CarsdataGrid.AllowUserToResizeRows = false;
            this.CarsdataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CarsdataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.CarsdataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CarsdataGrid.Location = new System.Drawing.Point(6, 6);
            this.CarsdataGrid.Name = "CarsdataGrid";
            this.CarsdataGrid.ReadOnly = true;
            this.CarsdataGrid.RowHeadersVisible = false;
            this.CarsdataGrid.Size = new System.Drawing.Size(1036, 421);
            this.CarsdataGrid.TabIndex = 0;
            // 
            // CarComplectControl
            // 
            this.CarComplectControl.Controls.Add(this.button3);
            this.CarComplectControl.Controls.Add(this.groupBox1);
            this.CarComplectControl.Controls.Add(this.EquipmentdataGridView);
            this.CarComplectControl.Controls.Add(this.CarEquipmentGridView);
            this.CarComplectControl.Location = new System.Drawing.Point(4, 22);
            this.CarComplectControl.Name = "CarComplectControl";
            this.CarComplectControl.Padding = new System.Windows.Forms.Padding(3);
            this.CarComplectControl.Size = new System.Drawing.Size(1048, 462);
            this.CarComplectControl.TabIndex = 1;
            this.CarComplectControl.Text = "Комплектації";
            this.CarComplectControl.UseVisualStyleBackColor = true;
            this.CarComplectControl.Click += new System.EventHandler(this.CarComplectControl_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(890, 83);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(149, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Редагувати комплектації";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(884, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(161, 76);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Комплектації авто";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(6, 48);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(149, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Видалити";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(6, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(149, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Додати";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // EquipmentdataGridView
            // 
            this.EquipmentdataGridView.AllowUserToAddRows = false;
            this.EquipmentdataGridView.AllowUserToDeleteRows = false;
            this.EquipmentdataGridView.AllowUserToResizeColumns = false;
            this.EquipmentdataGridView.AllowUserToResizeRows = false;
            this.EquipmentdataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EquipmentdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.EquipmentdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EquipmentdataGridView.Location = new System.Drawing.Point(617, 6);
            this.EquipmentdataGridView.Name = "EquipmentdataGridView";
            this.EquipmentdataGridView.ReadOnly = true;
            this.EquipmentdataGridView.RowHeadersVisible = false;
            this.EquipmentdataGridView.Size = new System.Drawing.Size(261, 450);
            this.EquipmentdataGridView.TabIndex = 2;
            // 
            // CarEquipmentGridView
            // 
            this.CarEquipmentGridView.AllowUserToAddRows = false;
            this.CarEquipmentGridView.AllowUserToDeleteRows = false;
            this.CarEquipmentGridView.AllowUserToResizeColumns = false;
            this.CarEquipmentGridView.AllowUserToResizeRows = false;
            this.CarEquipmentGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CarEquipmentGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.CarEquipmentGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CarEquipmentGridView.Location = new System.Drawing.Point(6, 6);
            this.CarEquipmentGridView.Name = "CarEquipmentGridView";
            this.CarEquipmentGridView.ReadOnly = true;
            this.CarEquipmentGridView.RowHeadersVisible = false;
            this.CarEquipmentGridView.Size = new System.Drawing.Size(605, 450);
            this.CarEquipmentGridView.TabIndex = 1;
            this.CarEquipmentGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CarEquipmentGridView_CellContentClick);
            // 
            // UsersControl
            // 
            this.UsersControl.Controls.Add(this.button4);
            this.UsersControl.Controls.Add(this.button5);
            this.UsersControl.Controls.Add(this.UsersGridView);
            this.UsersControl.Location = new System.Drawing.Point(4, 22);
            this.UsersControl.Name = "UsersControl";
            this.UsersControl.Padding = new System.Windows.Forms.Padding(3);
            this.UsersControl.Size = new System.Drawing.Size(1052, 487);
            this.UsersControl.TabIndex = 1;
            this.UsersControl.Text = "Користувачі";
            this.UsersControl.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(842, 458);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(123, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "Редагувати/Додати";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.Location = new System.Drawing.Point(971, 458);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "Видалити";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // UsersGridView
            // 
            this.UsersGridView.AllowUserToAddRows = false;
            this.UsersGridView.AllowUserToDeleteRows = false;
            this.UsersGridView.AllowUserToResizeColumns = false;
            this.UsersGridView.AllowUserToResizeRows = false;
            this.UsersGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UsersGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.UsersGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.UsersGridView.Location = new System.Drawing.Point(3, 6);
            this.UsersGridView.Name = "UsersGridView";
            this.UsersGridView.ReadOnly = true;
            this.UsersGridView.RowHeadersVisible = false;
            this.UsersGridView.Size = new System.Drawing.Size(1046, 446);
            this.UsersGridView.TabIndex = 1;
            // 
            // CustomersControl
            // 
            this.CustomersControl.Controls.Add(this.button6);
            this.CustomersControl.Controls.Add(this.button7);
            this.CustomersControl.Controls.Add(this.CustomersGridView);
            this.CustomersControl.Location = new System.Drawing.Point(4, 22);
            this.CustomersControl.Name = "CustomersControl";
            this.CustomersControl.Padding = new System.Windows.Forms.Padding(3);
            this.CustomersControl.Size = new System.Drawing.Size(1052, 487);
            this.CustomersControl.TabIndex = 2;
            this.CustomersControl.Text = "Клієнти";
            this.CustomersControl.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.Location = new System.Drawing.Point(842, 458);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(123, 23);
            this.button6.TabIndex = 8;
            this.button6.Text = "Редагувати/Додати";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button7.Location = new System.Drawing.Point(971, 458);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 7;
            this.button7.Text = "Видалити";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // CustomersGridView
            // 
            this.CustomersGridView.AllowUserToAddRows = false;
            this.CustomersGridView.AllowUserToDeleteRows = false;
            this.CustomersGridView.AllowUserToResizeColumns = false;
            this.CustomersGridView.AllowUserToResizeRows = false;
            this.CustomersGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CustomersGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.CustomersGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CustomersGridView.Location = new System.Drawing.Point(3, 6);
            this.CustomersGridView.Name = "CustomersGridView";
            this.CustomersGridView.ReadOnly = true;
            this.CustomersGridView.RowHeadersVisible = false;
            this.CustomersGridView.Size = new System.Drawing.Size(1046, 446);
            this.CustomersGridView.TabIndex = 6;
            // 
            // SaleControl
            // 
            this.SaleControl.Controls.Add(this.button14);
            this.SaleControl.Controls.Add(this.button13);
            this.SaleControl.Controls.Add(this.groupBox8);
            this.SaleControl.Controls.Add(this.groupBox7);
            this.SaleControl.Controls.Add(this.groupBox6);
            this.SaleControl.Controls.Add(this.groupBox5);
            this.SaleControl.Controls.Add(this.groupBox4);
            this.SaleControl.Controls.Add(this.label11);
            this.SaleControl.Controls.Add(this.SellingEquipdataGridView);
            this.SaleControl.Controls.Add(this.label2);
            this.SaleControl.Controls.Add(this.label1);
            this.SaleControl.Controls.Add(this.groupBox3);
            this.SaleControl.Controls.Add(this.groupBox2);
            this.SaleControl.Controls.Add(this.SellingCarsdataGridView);
            this.SaleControl.Controls.Add(this.SellingCustomersdataGridView);
            this.SaleControl.Controls.Add(this.button8);
            this.SaleControl.Location = new System.Drawing.Point(4, 22);
            this.SaleControl.Name = "SaleControl";
            this.SaleControl.Padding = new System.Windows.Forms.Padding(3);
            this.SaleControl.Size = new System.Drawing.Size(1052, 487);
            this.SaleControl.TabIndex = 3;
            this.SaleControl.Text = "Продаж";
            this.SaleControl.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button14.Location = new System.Drawing.Point(913, 432);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(133, 23);
            this.button14.TabIndex = 15;
            this.button14.Text = "Перегляд договорів";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button13
            // 
            this.button13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button13.Location = new System.Drawing.Point(913, 458);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(133, 23);
            this.button13.TabIndex = 14;
            this.button13.Text = "Формування договору";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.button11);
            this.groupBox8.Controls.Add(this.button12);
            this.groupBox8.Controls.Add(this.dataGridView1);
            this.groupBox8.Location = new System.Drawing.Point(263, 383);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(319, 98);
            this.groupBox8.TabIndex = 13;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Додаткові послуги";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(238, 65);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(74, 25);
            this.button11.TabIndex = 11;
            this.button11.Text = "Видалити";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(238, 16);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(74, 25);
            this.button12.TabIndex = 10;
            this.button12.Text = "Додати";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 16);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(226, 76);
            this.dataGridView1.TabIndex = 9;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.textBox18);
            this.groupBox7.Location = new System.Drawing.Point(588, 429);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(166, 52);
            this.groupBox7.TabIndex = 12;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Ціна всього договору";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(6, 18);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(154, 20);
            this.textBox18.TabIndex = 28;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.textBox17);
            this.groupBox6.Location = new System.Drawing.Point(588, 383);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(166, 46);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Ціна авто з комплектаціями";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(6, 16);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(154, 20);
            this.textBox17.TabIndex = 28;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.button10);
            this.groupBox5.Controls.Add(this.button9);
            this.groupBox5.Controls.Add(this.SelectedSellingEquipdataGridView);
            this.groupBox5.Location = new System.Drawing.Point(731, 279);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(313, 98);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Вибрана(і) комплектація(ї)";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(233, 67);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(74, 25);
            this.button10.TabIndex = 11;
            this.button10.Text = "Видалити";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(233, 15);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(74, 25);
            this.button9.TabIndex = 10;
            this.button9.Text = "Додати";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // SelectedSellingEquipdataGridView
            // 
            this.SelectedSellingEquipdataGridView.AllowUserToAddRows = false;
            this.SelectedSellingEquipdataGridView.AllowUserToDeleteRows = false;
            this.SelectedSellingEquipdataGridView.AllowUserToResizeColumns = false;
            this.SelectedSellingEquipdataGridView.AllowUserToResizeRows = false;
            this.SelectedSellingEquipdataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SelectedSellingEquipdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SelectedSellingEquipdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SelectedSellingEquipdataGridView.Location = new System.Drawing.Point(6, 16);
            this.SelectedSellingEquipdataGridView.Name = "SelectedSellingEquipdataGridView";
            this.SelectedSellingEquipdataGridView.ReadOnly = true;
            this.SelectedSellingEquipdataGridView.RowHeadersVisible = false;
            this.SelectedSellingEquipdataGridView.Size = new System.Drawing.Size(221, 76);
            this.SelectedSellingEquipdataGridView.TabIndex = 9;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.textBox5);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.textBox6);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.textBox7);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.textBox8);
            this.groupBox4.Location = new System.Drawing.Point(3, 380);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(253, 104);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Поточний користувач";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(129, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Дата народження";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(6, 32);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(119, 20);
            this.textBox5.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 57);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Побатькові";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(131, 32);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(118, 20);
            this.textBox6.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(129, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Прізвище";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(5, 73);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(119, 20);
            this.textBox7.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Імя";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(130, 73);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(118, 20);
            this.textBox8.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(922, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Копмлектації";
            // 
            // SellingEquipdataGridView
            // 
            this.SellingEquipdataGridView.AllowUserToAddRows = false;
            this.SellingEquipdataGridView.AllowUserToDeleteRows = false;
            this.SellingEquipdataGridView.AllowUserToResizeColumns = false;
            this.SellingEquipdataGridView.AllowUserToResizeRows = false;
            this.SellingEquipdataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SellingEquipdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SellingEquipdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SellingEquipdataGridView.Location = new System.Drawing.Point(871, 18);
            this.SellingEquipdataGridView.Name = "SellingEquipdataGridView";
            this.SellingEquipdataGridView.ReadOnly = true;
            this.SellingEquipdataGridView.RowHeadersVisible = false;
            this.SellingEquipdataGridView.Size = new System.Drawing.Size(175, 255);
            this.SellingEquipdataGridView.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(592, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Авто";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(143, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Клієнти";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.textBox15);
            this.groupBox3.Controls.Add(this.textBox16);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.textBox14);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.textBox13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.textBox9);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.textBox10);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.textBox11);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.textBox12);
            this.groupBox3.Location = new System.Drawing.Point(263, 279);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(462, 98);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Вибраний автомобіль";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(339, 56);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 13);
            this.label20.TabIndex = 31;
            this.label20.Text = "Ціна";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(258, 72);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(78, 20);
            this.textBox15.TabIndex = 28;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(342, 72);
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(114, 20);
            this.textBox16.TabIndex = 30;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(339, 17);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 29;
            this.label19.Text = "Тип палива";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(255, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "Потужність";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(128, 72);
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(124, 20);
            this.textBox14.TabIndex = 26;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(255, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "Рік випуску";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(6, 72);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(116, 20);
            this.textBox13.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(125, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Тип кузова";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(6, 32);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(116, 20);
            this.textBox9.TabIndex = 22;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Колір";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(128, 32);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(124, 20);
            this.textBox10.TabIndex = 20;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(125, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Модель";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(258, 33);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(78, 20);
            this.textBox11.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Марка";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(342, 33);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(114, 20);
            this.textBox12.TabIndex = 16;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Location = new System.Drawing.Point(3, 279);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(254, 98);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Вибраний клієнт";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(127, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Дата народження";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(130, 72);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(118, 20);
            this.textBox4.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Побатькові";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(6, 72);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(118, 20);
            this.textBox3.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(127, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Прізвище";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(130, 31);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(118, 20);
            this.textBox2.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Імя";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 31);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(118, 20);
            this.textBox1.TabIndex = 0;
            // 
            // SellingCarsdataGridView
            // 
            this.SellingCarsdataGridView.AllowUserToAddRows = false;
            this.SellingCarsdataGridView.AllowUserToDeleteRows = false;
            this.SellingCarsdataGridView.AllowUserToResizeColumns = false;
            this.SellingCarsdataGridView.AllowUserToResizeRows = false;
            this.SellingCarsdataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SellingCarsdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SellingCarsdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SellingCarsdataGridView.Location = new System.Drawing.Point(368, 18);
            this.SellingCarsdataGridView.Name = "SellingCarsdataGridView";
            this.SellingCarsdataGridView.ReadOnly = true;
            this.SellingCarsdataGridView.RowHeadersVisible = false;
            this.SellingCarsdataGridView.Size = new System.Drawing.Size(497, 255);
            this.SellingCarsdataGridView.TabIndex = 2;
            this.SellingCarsdataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SellingCarsdataGridView_CellContentClick);
            // 
            // SellingCustomersdataGridView
            // 
            this.SellingCustomersdataGridView.AllowUserToAddRows = false;
            this.SellingCustomersdataGridView.AllowUserToDeleteRows = false;
            this.SellingCustomersdataGridView.AllowUserToResizeColumns = false;
            this.SellingCustomersdataGridView.AllowUserToResizeRows = false;
            this.SellingCustomersdataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SellingCustomersdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SellingCustomersdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SellingCustomersdataGridView.Location = new System.Drawing.Point(6, 18);
            this.SellingCustomersdataGridView.Name = "SellingCustomersdataGridView";
            this.SellingCustomersdataGridView.ReadOnly = true;
            this.SellingCustomersdataGridView.RowHeadersVisible = false;
            this.SellingCustomersdataGridView.Size = new System.Drawing.Size(356, 255);
            this.SellingCustomersdataGridView.TabIndex = 1;
            this.SellingCustomersdataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SellingCustomersdataGridView_CellContentClick);
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button8.Location = new System.Drawing.Point(807, 458);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(100, 23);
            this.button8.TabIndex = 0;
            this.button8.Text = "Друк договору";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Visible = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.LightGray;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NameStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 543);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1060, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // NameStatusLabel
            // 
            this.NameStatusLabel.Name = "NameStatusLabel";
            this.NameStatusLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.NameStatusLabel.Size = new System.Drawing.Size(104, 17);
            this.NameStatusLabel.Text = "Name Curent User";
            this.NameStatusLabel.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // databaseBindingSource
            // 
            this.databaseBindingSource.DataSource = typeof(System.Data.Entity.Database);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 565);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ControlPanel);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Продаж авто";
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ControlPanel.ResumeLayout(false);
            this.CarControl.ResumeLayout(false);
            this.CarsControlPanel.ResumeLayout(false);
            this.CarControlAll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CarsdataGrid)).EndInit();
            this.CarComplectControl.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentdataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarEquipmentGridView)).EndInit();
            this.UsersControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UsersGridView)).EndInit();
            this.CustomersControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CustomersGridView)).EndInit();
            this.SaleControl.ResumeLayout(false);
            this.SaleControl.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SelectedSellingEquipdataGridView)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SellingEquipdataGridView)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SellingCarsdataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellingCustomersdataGridView)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.databaseBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem вихідToolStripMenuItem;
        private System.Windows.Forms.TabControl ControlPanel;
        private System.Windows.Forms.TabPage CarControl;
        private System.Windows.Forms.TabPage UsersControl;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel NameStatusLabel;
        private System.Windows.Forms.TabControl CarsControlPanel;
        private System.Windows.Forms.TabPage CarControlAll;
        private System.Windows.Forms.TabPage CarComplectControl;
        private System.Windows.Forms.TabPage CustomersControl;
        private System.Windows.Forms.TabPage SaleControl;
        private System.Windows.Forms.DataGridView CarsdataGrid;
        private System.Windows.Forms.BindingSource databaseBindingSource;
        private System.Windows.Forms.Button DeleteCar;
        private System.Windows.Forms.Button ChangeCar;
        private System.Windows.Forms.DataGridView EquipmentdataGridView;
        private System.Windows.Forms.DataGridView CarEquipmentGridView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView UsersGridView;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.DataGridView CustomersGridView;
        private System.Windows.Forms.ToolStripMenuItem проПрограмуToolStripMenuItem;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView SellingCarsdataGridView;
        private System.Windows.Forms.DataGridView SellingCustomersdataGridView;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.DataGridView SelectedSellingEquipdataGridView;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView SellingEquipdataGridView;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Button button14;

    }
}

