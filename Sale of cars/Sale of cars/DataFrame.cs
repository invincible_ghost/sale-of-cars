﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sale_of_cars
{
    public class DataFrame
    {
        public static string CarID { get; set; }
        public static string Car_Identification_id { get; set; }
        public static string UserID { get; set; }
        public static string UserResidenceID { get; set; }
        public static string CustomerID { get; set; }
        public static string CustomerResidenceID { get; set; }
        public static string CarPriceWithEquip { get; set; }
        public static string PriceOfDoc { get; set; }
    }
}
