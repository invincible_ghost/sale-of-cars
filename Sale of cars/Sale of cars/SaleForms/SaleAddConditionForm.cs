﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars.SaleForms
{
    public partial class SaleAddConditionForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        public SaleAddConditionForm()
        {
            InitializeComponent();
        }

        public void DataGridLoadData()
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            //Заповлення данимим датагріда
            SqlDataAdapter da = new SqlDataAdapter("select *  from [Additional conditions]", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Additional conditions]");
            dataGridView1.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Назва";
            dataGridView1.Columns[2].HeaderText = "Ціна";
        }
        private void SaleAddConditionForm_Load(object sender, EventArgs e)
        {
            DataGridLoadData();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            SqlCommand cmd = new SqlCommand("Insert [Additional conditions]" +
            " ([Additional conditions].[Name], [Additional conditions].[Price]) Values (@Назва1, @Ціна1)", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@Назва1";
            param.Value = textBox1.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Ціна1";
            param.Value = textBox2.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();

            db.Close();

            DataGridLoadData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            SqlCommand cmd = new SqlCommand("Update [Additional conditions]"
            + " Set [Additional conditions].[Name] =  @Назва1, [Additional conditions].[Price] = @Ціна1"
            + " where [Additional conditions].id = " + dataGridView1.CurrentRow.Cells[0].Value, db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@Назва1";
            param.Value = textBox1.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Ціна1";
            param.Value = textBox2.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();

            db.Close();

            DataGridLoadData();
        }
    }
}
