﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars.SaleForms
{
    public partial class SaleAllDocForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        public SaleAllDocForm()
        {
            InitializeComponent();
        }

        private void SaleAllDocForm_Load(object sender, EventArgs e)
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("SELECT [Sales documents].[id], [Users].[Name], [Users].[Surname], [Users].[Last_name], [Users].[Date_of_birth],"
	        +" [Customers].[Name], [Customers].[Surname], [Customers].[Last_name], [Customers].[Date_of_birth],"
	        +" [Cars Marks].[Name], [Model].[Name], [Body type].[Name], [Car color].[Name], [Type fuel].[Name],"
	        +" [Cars].[Price], [Data_of_signing], [Signing_price]"
            +" FROM [Sales documents]"
            +" Inner join [Users] on [Sales documents].[User_id] = [Users].[id]"

            +" Inner join [Cars] on [Sales documents].[Car_id] = [Cars].[Id]"
            +" Inner join [Cars Marks] on Cars.[Mark] = [Cars Marks].Id"
            +" Inner join Model on Cars.Model = [Model].Id"
            +" Inner join [Body type] on Cars.Body_type_id = [Body type].Id"
            +" Inner join [Car color] on Cars.Color_id = [Car color].Id"
            +" Inner join [Type fuel] on Cars.Fuel_id = [Type fuel].Id"

            +" Inner join [Customers] on [Sales documents].Customer_id = [Customers].[Id]", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Sales documents]");
            AllDocumentdataGridView.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            AllDocumentdataGridView.Columns[0].Visible = false;
            AllDocumentdataGridView.Columns[1].HeaderText = "Імя Кор.";
            AllDocumentdataGridView.Columns[2].HeaderText = "Прізвище Кор.";
            AllDocumentdataGridView.Columns[3].HeaderText = "Побатькові Кор.";
            AllDocumentdataGridView.Columns[4].HeaderText = "Дата народження Кор.";
            AllDocumentdataGridView.Columns[5].HeaderText = "Імя Клі.";
            AllDocumentdataGridView.Columns[6].HeaderText = "Прізвище Клі.";
            AllDocumentdataGridView.Columns[7].HeaderText = "Побатькові Клі.";
            AllDocumentdataGridView.Columns[8].HeaderText = "Дата народження Клі.";
            AllDocumentdataGridView.Columns[9].HeaderText = "Марка";
            AllDocumentdataGridView.Columns[10].HeaderText = "Модель";
            AllDocumentdataGridView.Columns[11].HeaderText = "Кузов";
            AllDocumentdataGridView.Columns[12].HeaderText = "Колір";
            AllDocumentdataGridView.Columns[13].HeaderText = "Тип палива";
            AllDocumentdataGridView.Columns[14].HeaderText = "Ціна авто";
            AllDocumentdataGridView.Columns[15].HeaderText = "Дата укладання";
            AllDocumentdataGridView.Columns[16].HeaderText = "Загальна ціна";



          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DocumentForm NewFrom = new DocumentForm();
            NewFrom.ShowDialog();
        }

        private void AllDocumentdataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            //Заповлення данимим датагріда
            SqlConnection db1 = new SqlConnection(ConnectionString);
            db1.Open();
            SqlDataAdapter da1 = new SqlDataAdapter("select [Additional equipment].[Name], [Additional equipment].Price from [Sale Car Equipment]"
            +" inner join [Additional equipment] on [Sale Car Equipment].[Additional_equipment_id] = [Additional equipment].id"
            + " where [Sale Car Equipment].[Sales_document_id] = " + AllDocumentdataGridView.CurrentRow.Cells[0].Value.ToString(), db1);
            SqlCommandBuilder cb1 = new SqlCommandBuilder(da1);
            DataSet ds1 = new DataSet();
            da1.Fill(ds1, "[Sale Car Equipment]");
            dataGridView2.DataSource = ds1.Tables[0];
            db1.Close();

            //Преіменовування хедерів датагріда
            dataGridView2.Columns[0].HeaderText = "Назва";
            dataGridView2.Columns[1].HeaderText = "Ціна";

            //Заповлення данимим датагріда
            SqlConnection db3 = new SqlConnection(ConnectionString);
            db3.Open();
            SqlDataAdapter da3 = new SqlDataAdapter("select [Additional conditions].[Name], [Additional conditions].[Price] from [Sale conditions]"
            +" inner join [Additional conditions] on [Sale conditions].Condition_id = [Additional conditions].id"
            +" where [Sale conditions].Document_id = " + AllDocumentdataGridView.CurrentRow.Cells[0].Value.ToString(), db3);
            SqlCommandBuilder cb3 = new SqlCommandBuilder(da3);
            DataSet ds3 = new DataSet();
            da3.Fill(ds3, "[Sale Car Equipment]");
            dataGridView1.DataSource = ds3.Tables[0];
            db3.Close();

            //Преіменовування хедерів датагріда
            dataGridView1.Columns[0].HeaderText = "Назва";
            dataGridView1.Columns[1].HeaderText = "Ціна";
           
        }
    }
}
