﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars.SaleForms
{
    public partial class SaleConditionsForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        bool Loadedform = false;
        public string CondName = "";
        public SaleConditionsForm()
        {
            InitializeComponent();
        }
        private void SaleConditionsForm_Load(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            //заполнение датасета
            string query1 = "Select * from [Additional conditions]";
            DataSet dataset1 = new DataSet();
            SqlDataAdapter adapter1 = new SqlDataAdapter(query1, db);
            adapter1.Fill(dataset1);
            //отображение в послугах
            comboBox1.DataSource = dataset1.Tables[0];
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "id";
            CondName = dataset1.Tables[0].Rows[0].ItemArray[1].ToString();
            textBox1.Text = dataset1.Tables[0].Rows[0].ItemArray[2].ToString();
            Loadedform = true;
            db.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaleAddConditionForm NewForm = new SaleAddConditionForm();
            NewForm.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Добавлення
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd = new SqlCommand("Insert into [Conditions Frame] " +
            "([Cond_id], [Cond_Name], [Cond_Price]) " +
            "Values (@Cond_id1, @Cond_Name1, @Cond_Price1)", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@Cond_id1";
            param.Value = Convert.ToInt32(comboBox1.SelectedValue.ToString());
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Cond_Name1";
            param.Value = CondName;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Cond_Price1";
            param.Value = textBox1.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();
            db.Close();

            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Loadedform == true)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();

                //заполнение датасета
                string query3 = "Select [Additional conditions].Name, [Additional conditions].Price from [Additional conditions] " +
                                "where [Additional conditions].id = " + comboBox1.SelectedValue;

                DataSet dataset3 = new DataSet();
                SqlDataAdapter adapter3 = new SqlDataAdapter(query3, db);
                adapter3.Fill(dataset3);
                //отображение в comboBox1
                textBox1.Text = dataset3.Tables[0].Rows[0].ItemArray[1].ToString();
                CondName = dataset3.Tables[0].Rows[0].ItemArray[0].ToString();
                db.Close();
            }
        }

        private void SaleConditionsForm_Activated(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            //заполнение датасета
            string query1 = "Select * from [Additional conditions]";
            DataSet dataset1 = new DataSet();
            SqlDataAdapter adapter1 = new SqlDataAdapter(query1, db);
            adapter1.Fill(dataset1);
            //отображение в послугах
            comboBox1.DataSource = dataset1.Tables[0];
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "id";
            CondName = dataset1.Tables[0].Rows[0].ItemArray[1].ToString();
            textBox1.Text = dataset1.Tables[0].Rows[0].ItemArray[2].ToString();
            db.Close();
        }
    }
}
