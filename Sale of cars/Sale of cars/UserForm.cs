﻿using Sale_of_cars.UserForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars
{
    public partial class UserForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        public UserForm()
        {
            InitializeComponent();
        }

        public void DataGridLoadData()
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("Select [Roles table].id, [Roles table].[Role_Name] from [Roles table]"
            +" inner join [User Roles] on [Roles table].id = [User Roles].Role_id"
            +" where [User Roles].[User_id] = "+DataFrame.UserID, db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Roles table]");
            dataGridView1.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Назва ролі";
        }

        public void LoadDataOnForm()
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            #region Заповнення Combobox

            //заполнение датасета
            string query1 = "Select * from [Posts]";
            DataSet dataset1 = new DataSet();
            SqlDataAdapter adapter1 = new SqlDataAdapter(query1, db);
            adapter1.Fill(dataset1);
            //отображение в марка
            comboBox1.DataSource = dataset1.Tables[0];
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "id";

            #endregion

            #region Заповненн полів форми
            SqlDataAdapter da = new SqlDataAdapter("select distinct [Users].id, [Users].[Name], [Users].Surname, [Users].Last_name, [Users].Post_id,"
            +" [Users].Passport, [Users].[Login], [Users].[Password], [Users].Email, [Users].Date_of_birth, [Users].Place_of_residence_id,"
            +" [Place of residence].country, [Place of residence].region, [Place of residence].district,"
            +" [Place of residence].village, [Place of residence].city, [Place of residence].street,"
            +" [Place of residence].apartment_house from [Users]"
            +" inner join [Posts] on [Users].Post_id = [Posts].id"
            +" inner join [Place of residence] on [Users].Place_of_residence_id = [Place of residence].id where [Users].id = " + DataFrame.UserID, db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Users");

            comboBox1.SelectedValue = ds.Tables[0].Rows[0].ItemArray[4].ToString(); //Посада
            textBox1.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString(); //Імя
            textBox2.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString(); //Прізвище
            textBox3.Text = ds.Tables[0].Rows[0].ItemArray[3].ToString(); //Побатькові
            textBox4.Text = ds.Tables[0].Rows[0].ItemArray[5].ToString(); //Паспортні дані
            textBox5.Text = ds.Tables[0].Rows[0].ItemArray[6].ToString(); //Логін
            textBox6.Text = ds.Tables[0].Rows[0].ItemArray[7].ToString(); //Пароль
            textBox7.Text = ds.Tables[0].Rows[0].ItemArray[8].ToString(); //Email
            textBox8.Text = ds.Tables[0].Rows[0].ItemArray[9].ToString(); //Дата народження
            textBox9.Text = ds.Tables[0].Rows[0].ItemArray[11].ToString(); //Країна
            textBox10.Text = ds.Tables[0].Rows[0].ItemArray[12].ToString(); //Область 
            textBox11.Text = ds.Tables[0].Rows[0].ItemArray[13].ToString(); //Район
            textBox12.Text = ds.Tables[0].Rows[0].ItemArray[14].ToString(); //Село
            textBox13.Text = ds.Tables[0].Rows[0].ItemArray[15].ToString(); //Місто
            textBox14.Text = ds.Tables[0].Rows[0].ItemArray[16].ToString(); //Вулиця
            textBox15.Text = ds.Tables[0].Rows[0].ItemArray[17].ToString(); //Будинок

            #endregion

            db.Close();
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            LoadDataOnForm();

            DataGridLoadData();
        }

        private void посадиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserPostsForm NewForm = new UserPostsForm();
            NewForm.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            UserRolesForm NewForm = new UserRolesForm();
            NewForm.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string UserRoleDel = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            DialogResult dialogResult = MessageBox.Show("Ви впевнен що хочете видалити роль " + dataGridView1.CurrentRow.Cells[1].Value.ToString().Trim() +" у користувача?", "Видалення ролі користувача", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();

                SqlCommand cmd = new SqlCommand("Delete From [User Roles]" +
                " where [User Roles].[User_id] = @IDКористувача AND [User Roles].[Role_id] = @IDРолі", db);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@IDКористувача";
                param.Value = Convert.ToInt32(DataFrame.UserID);
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);
                param = new SqlParameter();
                param.ParameterName = "@IDРолі";
                param.Value = Convert.ToInt32(UserRoleDel);
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();

                db.Close();

                DataGridLoadData();
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }

        private void UserForm_Activated(object sender, EventArgs e)
        {
            DataGridLoadData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool UserChenged = false;
            bool UserResidenceChenged = false;
            //Update користувача
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd1 = new SqlCommand("Update [Users] "
            + " Set [Users].[Name] = @Name1, [Users].Last_name = @Last_name1, [Users].Surname = @Surname1, [Users].Post_id = @Post_id1, [Users].Passport = @Passport1, "
            + " [Users].[Login] = @Login1, [Users].[Password] = @Password1, [Users].Email = @Email1, [Users].Date_of_birth = @Date_of_birth1 "
            + " where [Users].id = " + DataFrame.UserID, db);
            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "@Name1";
            param1.Value = textBox1.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Surname1";
            param1.Value = textBox2.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Last_name1";
            param1.Value = textBox3.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Post_id1";
            param1.Value = Convert.ToInt32(comboBox1.SelectedValue.ToString());
            param1.SqlDbType = SqlDbType.Int;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Passport1";
            param1.Value = textBox4.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Login1";
            param1.Value = textBox5.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Password1";
            param1.Value = textBox6.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Email1";
            param1.Value = textBox7.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Date_of_birth1";
            param1.Value = textBox8.Text;
            param1.SqlDbType = SqlDbType.Date;
            cmd1.Parameters.Add(param1);
            cmd1.ExecuteNonQuery();
            UserChenged = true;
            db.Close();

            //Update місця проживання користувача
            SqlConnection db1 = new SqlConnection(ConnectionString);
            db1.Open();
            SqlCommand cmd = new SqlCommand("Update [Place of residence]"
            + " set [Place of residence].country = @country1, [Place of residence].region = @region1, [Place of residence].district = @district1, "
            + " [Place of residence].village = @village1, [Place of residence].city = @city1, [Place of residence].street = @street1, "
            + " [Place of residence].apartment_house = @apartment_house1 where [Place of residence].id = " + DataFrame.UserResidenceID, db1);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@country1";
            param.Value = textBox9.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@region1";
            param.Value = textBox10.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@district1";
            param.Value = textBox11.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@village1";
            param.Value = textBox12.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@city1";
            param.Value = textBox13.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@street1";
            param.Value = textBox14.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@apartment_house1";
            param.Value = textBox15.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();
            UserResidenceChenged = true;
            db1.Close();

            if (UserChenged == true && UserResidenceChenged == true)
            {
                MessageBox.Show("Користувач відредагований", "Виконано", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string CurentUserResident = "0";
            bool UserADD = false;
            //Добавлення даних коритсувача про проживання
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd = new SqlCommand("Insert into [Place of residence] " +
            "([country], [region], [district], [village], [city], [street], [apartment_house]) " +
            "Values (@country1, @region1, @district1, @village1, @city1, @street1, @apartment_house1)", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@country1";
            param.Value = textBox9.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@region1";
            param.Value = textBox10.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@district1";
            param.Value = textBox11.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@village1";
            param.Value = textBox12.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@city1";
            param.Value = textBox13.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@street1";
            param.Value = textBox14.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@apartment_house1";
            param.Value = textBox15.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();
            db.Close();

            // Знаходження id проживання користувача в базі даних
            string queryString = "Select distinct [Place of residence].[id] from [Place of residence] where [Place of residence].country = @country1 "
            + " and [Place of residence].region = @region1 and [Place of residence].district = @district1 and [Place of residence].village = @village1 and "
            + " [Place of residence].city = @city1 and [Place of residence].street = @street1 and [Place of residence].apartment_house = @apartment_house1";

            using (SqlConnection connection =
                new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@country1", textBox9.Text);
                command.Parameters.AddWithValue("@region1", textBox10.Text);
                command.Parameters.AddWithValue("@district1", textBox11.Text);
                command.Parameters.AddWithValue("@village1", textBox12.Text);
                command.Parameters.AddWithValue("@city1", textBox13.Text);
                command.Parameters.AddWithValue("@street1", textBox14.Text);
                command.Parameters.AddWithValue("@apartment_house1", textBox15.Text);

                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CurentUserResident = reader[0].ToString();
                }
                reader.Close();
                connection.Close();

            }

            //Добавлення користувача
            SqlConnection db2 = new SqlConnection(ConnectionString);
            db2.Open();
            SqlCommand cmd1 = new SqlCommand("Insert into [Users]" +
            "([Users].[Name], [Users].Surname, [Users].Last_name, [Users].Post_id,"
            +" [Users].Passport, [Users].[Login], [Users].[Password], [Users].Email, [Users].Date_of_birth, [Users].Place_of_residence_id)" +
            "Values (@Name1, @Surname1, @Last_name1, @Post_id1, @Passport1, @Login1, @Password1, @Email1, @Date_of_birth1, @Place_of_residence_id1)", db2);
            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "@Name1";
            param1.Value = textBox1.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Surname1";
            param1.Value = textBox2.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Last_name1";
            param1.Value = textBox3.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Post_id1";
            param1.Value = Convert.ToInt32(comboBox1.SelectedIndex.ToString());
            param1.SqlDbType = SqlDbType.Int;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Passport1";
            param1.Value = textBox4.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Login1";
            param1.Value = textBox5.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Password1";
            param1.Value = textBox6.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Email1";
            param1.Value = textBox7.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Date_of_birth1";
            param1.Value = textBox8.Text;
            param1.SqlDbType = SqlDbType.Date;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Place_of_residence_id1";
            param1.Value = Convert.ToInt32(CurentUserResident);
            param1.SqlDbType = SqlDbType.Int;
            cmd1.Parameters.Add(param1);
            
            cmd1.ExecuteNonQuery();
            UserADD = true;
            db2.Close();

            if (UserADD == true)
            {
                MessageBox.Show("Нового користувача додано в базу даних", "Виконано", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
