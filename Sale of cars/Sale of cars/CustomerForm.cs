﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars
{
    public partial class CustomerForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        public CustomerForm()
        {
            InitializeComponent();
        }

        private void CustomerForm_Load(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            SqlDataAdapter da = new SqlDataAdapter("select distinct [Customers].[Id], [Customers].[Name], [Customers].Surname, "
            +" [Customers].Last_name, [Customers].Home_num, [Customers].Phone_num, [Customers].Date_of_birth, "
            +" [Place of residence].country, [Place of residence].region, [Place of residence].district, "
            +" [Place of residence].village, [Place of residence].city, [Place of residence].street, "
            +" [Place of residence].apartment_house  from [Customers]"
            +" inner join [Place of residence] on [Customers].Place_of_residence_id = [Place of residence].id"
            + " where [Customers].[Id] = " + DataFrame.CustomerID, db); 
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Customers]");

            textBox1.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString(); //Імя
            textBox2.Text = ds.Tables[0].Rows[0].ItemArray[2].ToString(); //Прізвище
            textBox3.Text = ds.Tables[0].Rows[0].ItemArray[3].ToString(); //Побатькові
            textBox4.Text = ds.Tables[0].Rows[0].ItemArray[4].ToString(); //Домашній тел
            textBox5.Text = ds.Tables[0].Rows[0].ItemArray[5].ToString(); //Мобільний тел
            textBox6.Text = ds.Tables[0].Rows[0].ItemArray[6].ToString(); //Дата народження
            textBox7.Text = ds.Tables[0].Rows[0].ItemArray[7].ToString(); //Країна
            textBox8.Text = ds.Tables[0].Rows[0].ItemArray[8].ToString(); //Область
            textBox9.Text = ds.Tables[0].Rows[0].ItemArray[9].ToString(); //Район
            textBox10.Text = ds.Tables[0].Rows[0].ItemArray[10].ToString(); //Село
            textBox11.Text = ds.Tables[0].Rows[0].ItemArray[11].ToString(); //Місто
            textBox12.Text = ds.Tables[0].Rows[0].ItemArray[12].ToString(); //Вулиця
            textBox13.Text = ds.Tables[0].Rows[0].ItemArray[13].ToString(); //Будинок

            db.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool CustomerChenged = false;
            bool CustomerResidenceChenged = false;
            //Update користувача
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd1 = new SqlCommand("Update [Customers]"
            + " Set [Customers].[Name] = @Name1, [Customers].Last_name = @Last_name1, [Customers].Surname = @Surname1, [Customers].Home_num = @Home_num1, "
            + " [Customers].Phone_num = @Phone_num1, [Customers].Date_of_birth = @Date_of_birth1  where [Customers].Id = " + DataFrame.CustomerID, db);
            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "@Name1";
            param1.Value = textBox1.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Last_name1";
            param1.Value = textBox3.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Surname1";
            param1.Value = textBox2.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Home_num1";
            param1.Value = textBox4.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Phone_num1";
            param1.Value = textBox5.Text;
            param1.SqlDbType = SqlDbType.NVarChar;
            cmd1.Parameters.Add(param1);
            param1 = new SqlParameter();
            param1.ParameterName = "@Date_of_birth1";
            param1.Value = textBox6.Text;
            param1.SqlDbType = SqlDbType.Date;
            cmd1.Parameters.Add(param1);
            cmd1.ExecuteNonQuery();
            CustomerChenged = true;
            db.Close();

            //Update місця проживання користувача
            SqlConnection db1 = new SqlConnection(ConnectionString);
            db1.Open();
            SqlCommand cmd = new SqlCommand("Update [Place of residence]"
            + " set [Place of residence].country = @country1, [Place of residence].region = @region1, [Place of residence].district = @district1, "
            + " [Place of residence].village = @village1, [Place of residence].city = @city1, [Place of residence].street = @street1, "
            + " [Place of residence].apartment_house = @apartment_house1 where [Place of residence].id = " + DataFrame.CustomerResidenceID, db1);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@country1";
            param.Value = textBox7.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@region1";
            param.Value = textBox8.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@district1";
            param.Value = textBox9.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@village1";
            param.Value = textBox10.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@city1";
            param.Value = textBox11.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@street1";
            param.Value = textBox12.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@apartment_house1";
            param.Value = textBox13.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();
            CustomerResidenceChenged = true;
            db1.Close();

            if (CustomerChenged == true && CustomerResidenceChenged == true)
            {
                MessageBox.Show("Клієнт відредагований", "Виконано", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string CurentCustomerResident = "0";
            bool CustomerADD = false;
            //Добавлення даних клієнта про проживання
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd = new SqlCommand("Insert into [Place of residence] " +
            "([country], [region], [district], [village], [city], [street], [apartment_house]) " +
            "Values (@country1, @region1, @district1, @village1, @city1, @street1, @apartment_house1)", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@country1";
            param.Value = textBox7.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@region1";
            param.Value = textBox8.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@district1";
            param.Value = textBox9.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@village1";
            param.Value = textBox10.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@city1";
            param.Value = textBox11.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@street1";
            param.Value = textBox12.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@apartment_house1";
            param.Value = textBox13.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();
            db.Close();

            // Знаходження id проживання клієнта в базі даних
            string queryString = "Select distinct [Place of residence].[id] from [Place of residence] where [Place of residence].country = @country1 "
            + " and [Place of residence].region = @region1 and [Place of residence].district = @district1 and [Place of residence].village = @village1 and "
            + " [Place of residence].city = @city1 and [Place of residence].street = @street1 and [Place of residence].apartment_house = @apartment_house1";

            using (SqlConnection connection =
                new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@country1", textBox7.Text);
                command.Parameters.AddWithValue("@region1", textBox8.Text);
                command.Parameters.AddWithValue("@district1", textBox9.Text);
                command.Parameters.AddWithValue("@village1", textBox10.Text);
                command.Parameters.AddWithValue("@city1", textBox11.Text);
                command.Parameters.AddWithValue("@street1", textBox12.Text);
                command.Parameters.AddWithValue("@apartment_house1", textBox13.Text);

                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CurentCustomerResident = reader[0].ToString();
                    //textBox14.Text = reader[0].ToString();
                }
                reader.Close();
                connection.Close();

            }

                //Добавлення клієнта
                SqlConnection db2 = new SqlConnection(ConnectionString);
                db2.Open();
                SqlCommand cmd1 = new SqlCommand("Insert into [Customers]" +
                "([Customers].[Name], [Customers].[Last_name], [Customers].[Surname], [Customers].[Home_num], [Customers].[Phone_num], [Customers].[Date_of_birth], [Customers].[Place_of_residence_id])" +
                "Values (@Name1, @Last_name1, @Surname1, @Home_num1, @Phone_num1, @Date_of_birth1, @Place_of_residence_id1)", db2);
                SqlParameter param1 = new SqlParameter();
                param1.ParameterName = "@Name1";
                param1.Value = textBox1.Text;
                param1.SqlDbType = SqlDbType.NVarChar;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Last_name1";
                param1.Value = textBox3.Text;
                param1.SqlDbType = SqlDbType.NVarChar;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Surname1";
                param1.Value = textBox2.Text;
                param1.SqlDbType = SqlDbType.NVarChar;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Home_num1";
                param1.Value = textBox4.Text;
                param1.SqlDbType = SqlDbType.NVarChar;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Phone_num1";
                param1.Value = textBox5.Text;
                param1.SqlDbType = SqlDbType.NVarChar;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Date_of_birth1";
                param1.Value = textBox6.Text;
                param1.SqlDbType = SqlDbType.Date;
                cmd1.Parameters.Add(param1);
                param1 = new SqlParameter();
                param1.ParameterName = "@Place_of_residence_id1";
                param1.Value = Convert.ToInt32(CurentCustomerResident);
                param1.SqlDbType = SqlDbType.Int;
                cmd1.Parameters.Add(param1);
                cmd1.ExecuteNonQuery();
                CustomerADD = true;
                db2.Close();

            if (CustomerADD == true)
            {
                MessageBox.Show("Нового клієнта додано в базу даних", "Виконано", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
