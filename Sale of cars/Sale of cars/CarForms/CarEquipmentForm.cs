﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars.CarForms
{
    public partial class CarEquipmentForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        bool Loadedform = false;
        public CarEquipmentForm()
        {
            InitializeComponent();
        }
        private void CarEquipmentForm_Load(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            //заполнение датасета
            string query1 = "Select * from [Additional equipment]";
            DataSet dataset1 = new DataSet();
            SqlDataAdapter adapter1 = new SqlDataAdapter(query1, db);
            adapter1.Fill(dataset1);
            //отображение в марка
            comboBox1.DataSource = dataset1.Tables[0];
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "id";
            textBox1.Text = dataset1.Tables[0].Rows[0].ItemArray[2].ToString();
            Loadedform = true;
            db.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd = new SqlCommand("Insert into [Car equipment] " +
            "(Car_id , Equipment_id) " +
            "Values (@Car_id1 , @Equipment_id1)", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@Car_id1";
            param.Value = Convert.ToInt32(DataFrame.CarID);
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Equipment_id1";
            param.Value = Convert.ToInt32(comboBox1.SelectedValue.ToString());
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();
            db.Close();

            this.Close();
        }

     
        private void CarEquipmentForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Loadedform == true)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();

                //заполнение датасета
                string query3 = "Select [Additional equipment].Price from [Additional equipment] " +
                                "where [Additional equipment].id = " + comboBox1.SelectedValue;

                DataSet dataset3 = new DataSet();
                SqlDataAdapter adapter3 = new SqlDataAdapter(query3, db);
                adapter3.Fill(dataset3);
                //отображение в comboBox1
                textBox1.Text = dataset3.Tables[0].Rows[0].ItemArray[0].ToString();

                db.Close();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
