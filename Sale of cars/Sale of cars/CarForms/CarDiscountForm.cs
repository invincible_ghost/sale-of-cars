﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars.CarForms
{
    public partial class CarDiscountForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        public CarDiscountForm()
        {
            InitializeComponent();
        }
        public void DataGridLoadData()
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            //Заповлення данимим датагріда
            SqlDataAdapter da = new SqlDataAdapter("select [Discount].Id, [Discount].[NameDiscount], [Discount].[Per_cent_of_Discount], [Discount].[Start_Date], [Discount].[End_Date]  from [Discount]", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Discount");
            dataGridView1.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Назва";
            dataGridView1.Columns[2].HeaderText = "Скидка";
            dataGridView1.Columns[3].HeaderText = "Дата початку";
            dataGridView1.Columns[4].HeaderText = "Дата кінця";
        }

        private void CarDiscountForm_Load(object sender, EventArgs e)
        {
            DataGridLoadData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            SqlCommand cmd = new SqlCommand("Insert Discount" +
            " ([Discount].[NameDiscount], [Discount].[Per_cent_of_Discount], [Discount].[Start_Date], [Discount].[End_Date]) Values ( @Назва_скидки, @Скидка, @Початок, @Кінець )", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@Назва_скидки";
            param.Value = textBox1.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Скидка";
            param.Value = Convert.ToInt32(textBox2.Text);
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Початок";
            param.Value = textBox3.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Кінець";
            param.Value = textBox4.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();

            db.Close();

            DataGridLoadData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            SqlCommand cmd = new SqlCommand("Update Discount" +
            " Set [Discount].[NameDiscount] =  @Назва_скидки, [Discount].[Per_cent_of_Discount] = @Скидка,"
            + " [Discount].[Start_Date] = @Початок, [Discount].[End_Date] = @Кінець "
            + " where [Discount].id = " + dataGridView1.CurrentRow.Cells[0].Value, db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@Назва_скидки";
            param.Value = textBox1.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Скидка";
            param.Value = Convert.ToInt32(textBox2.Text);
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Початок";
            param.Value = textBox3.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Кінець";
            param.Value = textBox4.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();

            db.Close();

            DataGridLoadData();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            textBox3.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox4.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            //comboBox1.SelectedText = dataGridView1.CurrentRow.Cells[2].Value.ToString();
        }


        private void CarDiscountForm_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }
}
