﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars.CarForms
{
    public partial class CarModelForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";

        public CarModelForm()
        {
            InitializeComponent();
        }

        public void DataGridLoadData()
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            //Заповлення данимим датагріда
            SqlDataAdapter da = new SqlDataAdapter("select [Model].Id, [Model].[Name], [Cars Marks].[Name]  from [Model]"
            + " inner join [Cars Marks] on [Model].Id_mark = [Cars Marks].Id", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Model");
            dataGridView1.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Модель";
            dataGridView1.Columns[2].HeaderText = "Марка";
        }

        private void CarModelForm_Load(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            //заполнение датасета
            string query1 = "Select * from [Cars Marks]";
            DataSet dataset1 = new DataSet();
            SqlDataAdapter adapter1 = new SqlDataAdapter(query1, db);
            adapter1.Fill(dataset1);
            //отображение в марка
            comboBox1.DataSource = dataset1.Tables[0];
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "Id";

            db.Close();

            DataGridLoadData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            SqlCommand cmd = new SqlCommand("Insert Model" +
            " ([Model].[Name],  [Model].Id_mark) Values ( @МодельАвто, @МаркаID )", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@МодельАвто";
            param.Value = textBox1.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@МаркаID";
            param.Value = comboBox1.SelectedValue;
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();

            db.Close();

            DataGridLoadData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            SqlCommand cmd = new SqlCommand("Update Model" +
            " Set [Model].[Name] =  @МодельАвто, [Model].Id_mark = @МаркаID"
            + " where [Model].Id = "+ dataGridView1.CurrentRow.Cells[0].Value, db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@МодельАвто";
            param.Value = textBox1.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@МаркаID";
            param.Value = comboBox1.SelectedValue;
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();

            db.Close();

            DataGridLoadData();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            //comboBox1.SelectedText = dataGridView1.CurrentRow.Cells[2].Value.ToString();
        }

        private void CarModelForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

    }
}
