﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars.CarForms
{
    public partial class CarFuelForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        public CarFuelForm()
        {
            InitializeComponent();
        }
        public void DataGridLoadData()
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            //Заповлення данимим датагріда
            SqlDataAdapter da = new SqlDataAdapter("select [Type fuel].Id, [Type fuel].[Name]  from [Type fuel]", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Type fuel]");
            dataGridView1.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Тип палива";
        }

        private void CarFuelForm_Load(object sender, EventArgs e)
        {
            DataGridLoadData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            SqlCommand cmd = new SqlCommand("Insert [Type fuel]" +
            " ([Type fuel].[Name]) Values (@Паливо)", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@Паливо";
            param.Value = textBox1.Text;
            param.SqlDbType = SqlDbType.NChar;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();

            db.Close();

            DataGridLoadData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();

            SqlCommand cmd = new SqlCommand("Update [Type fuel]" +
            " Set [Type fuel].[Name] =  @Паливо  where [Type fuel].Id = " + dataGridView1.CurrentRow.Cells[0].Value, db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@Паливо";
            param.Value = textBox1.Text;
            param.SqlDbType = SqlDbType.NChar;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();

            db.Close();

            DataGridLoadData();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
        }


        private void CarFuelForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }
    }
}
