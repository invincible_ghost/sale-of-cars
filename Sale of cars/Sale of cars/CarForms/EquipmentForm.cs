﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sale_of_cars.CarForms
{
    public partial class EquipmentForm : Form
    {
        public string ConnectionString = @"Data Source=COMPUTER;Initial Catalog=Kazakov;Integrated Security=True";
        public EquipmentForm()
        {
            InitializeComponent();
        }

        public void DataGridLoadDataEquipment()
        {
            //Заповлення данимим датагріда
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlDataAdapter da = new SqlDataAdapter("Select [Additional equipment].id, [Additional equipment].[Name], [Additional equipment].Price from [Additional equipment]", db);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "[Additional equipment]");
            EquipmentdataGridView.DataSource = ds.Tables[0];
            db.Close();

            //Преіменовування хедерів датагріда
            EquipmentdataGridView.Columns[0].Visible = false;
            EquipmentdataGridView.Columns[1].HeaderText = "Назва";
            EquipmentdataGridView.Columns[2].HeaderText = "Ціна";
        }

        private void EquipmentForm_Load(object sender, EventArgs e)
        {
            DataGridLoadDataEquipment();
        }

        private void EquipmentForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void EquipmentdataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = EquipmentdataGridView.CurrentRow.Cells[1].Value.ToString();
            textBox2.Text = EquipmentdataGridView.CurrentRow.Cells[2].Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Ви впевнен що хочете видалити копмлектацію '" + EquipmentdataGridView.CurrentRow.Cells[1].Value.ToString().Trim() + "' за ціною '" + EquipmentdataGridView.CurrentRow.Cells[2].Value.ToString().Trim() + "' з бази даних?", "Видалення комплектації", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                SqlConnection db = new SqlConnection(ConnectionString);
                db.Open();

                SqlCommand cmd = new SqlCommand("Delete From [Additional equipment]" +
                " where [Additional equipment].Id = @IDКомплектації", db);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@IDКомплектації";
                param.Value = Convert.ToInt32(EquipmentdataGridView.CurrentRow.Cells[0].Value.ToString().Trim());
                param.SqlDbType = SqlDbType.Int;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();

                db.Close();

                DataGridLoadDataEquipment();
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd = new SqlCommand("Insert into [Additional equipment] " +
            "([Name] ,[Price]) " +
            "Values (@Name1 , @Price1)", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@Name1";
            param.Value = textBox1.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Price1";
            param.Value = Convert.ToSingle(textBox2.Text);
            param.SqlDbType = SqlDbType.Float;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();
            db.Close();

            DataGridLoadDataEquipment();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bool CarChenged = false;

            SqlConnection db = new SqlConnection(ConnectionString);
            db.Open();
            SqlCommand cmd = new SqlCommand("Update [Additional equipment] " +
            " Set [Additional equipment].[Name] = @Name1 ,  [Additional equipment].[Price] = @Price1 where [Additional equipment].[id] = @ID1", db);
            SqlParameter param = new SqlParameter();
            param.ParameterName = "@Name1";
            param.Value = textBox1.Text;
            param.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@Price1";
            param.Value = Convert.ToSingle(textBox2.Text);
            param.SqlDbType = SqlDbType.Float;
            cmd.Parameters.Add(param);
            param = new SqlParameter();
            param.ParameterName = "@ID1";
            param.Value = Convert.ToInt32(EquipmentdataGridView.CurrentRow.Cells[0].Value.ToString().Trim());
            param.SqlDbType = SqlDbType.Int;
            cmd.Parameters.Add(param);
            cmd.ExecuteNonQuery();
            CarChenged = true;
            db.Close();
            DataGridLoadDataEquipment();
            if (CarChenged == true)
            {
                MessageBox.Show("Комплектація відредагована", "Виконано", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
